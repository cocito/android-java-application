package it.cocito.chef;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import it.cocito.chef.fragments.AccountFragment;
import it.cocito.chef.fragments.DietFragment;
import it.cocito.chef.fragments.ListFragment;
import it.cocito.chef.fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity {
    /**
     * La navigation bar
     */
    BottomNavigationView navigation;

    /**
     * Il listener della navigation bar dell'activity
     */
    private BottomNavigationView.OnNavigationItemSelectedListener navigationListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            selectFrag(item);
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Richiamo il metodo della classe madre per generare gli oggetti a schermo
        super.onCreate(savedInstanceState);
        // Imposto l'activity che verrà visualizzata alla creazione della pagina
        setContentView(R.layout.activity_main);

        setupNav(savedInstanceState);
    }

    private void setupNav(Bundle savedInstanceState) {
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        if (navigation != null) {
            Menu menu = navigation.getMenu();
            if (savedInstanceState == null)
                selectFrag(menu.getItem(1));

            navigation.setOnNavigationItemSelectedListener(navigationListener);
        }

    }

    /**
     * Seleziona il frammento da visualizzare
     *
     * @param item -> la scheda selezionata
     */
    protected void selectFrag(MenuItem item) {
        DietFragment df = new DietFragment();

        item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.action_account:
                pushFrag(new AccountFragment());
                break;
            case R.id.action_list:
                ListFragment f = new ListFragment();
                pushFrag(f);
                break;
            case R.id.action_diet:
                pushFrag(df);
                break;
            case R.id.action_settings:
                pushFrag(new SettingsFragment());
                break;
        }
    }

    /**
     * Cambia il frammento nel container
     *
     * @param frag -> istanza del nuovo frammento da impostare
     */
    public void pushFrag(Fragment frag) {
        if (frag != null) {
            FragmentManager fm = getFragmentManager();
            if (fm != null) {
                FragmentTransaction ft = fm.beginTransaction();
                if (ft != null) {
                    ft.replace(R.id.content, frag);
                    ft.commit();
                }
            }
        }
    }

    /*
    /**
     * La larghezza dello schermo
     
    int width;
    /**
     * L'altezza dello schermo
     
    int height;
    /**
     * Il bottone di ricerca
     
    Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Richiamo il metodo della classe madre per generare gli oggetti a schermo
        super.onCreate(savedInstanceState);
        // Imposto l'activity che verrà visualizzata alla creazione della pagina
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
         Variabili Temporanee 
        int size;

        super.onWindowFocusChanged(hasFocus);

        // Dichiaro le costanti dello schermo sfruttando il WindowManager
        Point pt = new Point();
        getWindowManager().getDefaultDisplay().getSize(pt);
        this.width = pt.x;
        this.height = pt.y;

        // Ottengo il bottone di ricerca e lo salvo a livello dell'istanza
        btnSearch = (Button) findViewById(R.id.btnSearch);

        size = (int) (this.width * 0.2); // 20% della grandezza e la traduco in pixel
        ViewGroup.LayoutParams params = btnSearch.getLayoutParams();
        // Imposto le dimensioni uguali entrambe al 20% della larghezza dello schermo
        params.width = size;
        params.height = size;
        btnSearch.setLayoutParams(params); // Reinserisco i parametri nel bottone
    }*/
}
