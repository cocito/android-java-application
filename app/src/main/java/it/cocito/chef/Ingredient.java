package it.cocito.chef;

import java.util.AbstractMap;
import java.util.Map;

public class Ingredient {
    public String name;
    public float quantity;
    public String visualisedQuantity;
    public Map<String, Float> nutritionalValues;

    /**
     * Definisce un ingrediente indicandone nome, quantità
     * e valori nutrizionali
     *
     * @param name              nome dell'ingrediente
     * @param quantity          quantità dell'ingrediente
     * @param nutritionalValues valori nutrizionali dell'ingrediente
     */
    public Ingredient(String name, float quantity, Map<String, Float> nutritionalValues) {
        this.name = name;
        this.quantity = quantity;
        this.nutritionalValues = nutritionalValues;
    }

    public Ingredient() {
    }

    // GETTERS & SETTERS

    /**
     * Prende il nome dell'ingrediente
     *
     * @return restituisce il nome dell'ingrediente
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il nome dell'ingrediente
     *
     * @param name nome dell'ingrediente
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Prende la quantità dell'ingrediente
     *
     * @return restituisce la quantià dell'ingrediente
     */
    public float getQuantity() {
        return quantity;
    }

    /**
     * Imposta la quantità dell'ingrediente
     *
     * @param quantity quantità dell'ingrediente
     */
    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    /**
     * Prende la quantità visualizzata
     *
     * @return restituisce la quantità visualizzata
     */
    public String getVisualisedQuantity() {
        return visualisedQuantity;
    }

    /**
     * Imposta la quantità visualizzata
     *
     * @param visualizedQuantity
     */
    public void setVisualisedQuantity(String visualizedQuantity) {
        this.visualisedQuantity = visualizedQuantity;
    }

    /**
     * Prende i valori nutrizionali dell'ingrediente
     *
     * @return restituisce i valori nutrizionali dell'ingrediente
     */
    public Map<String, Float> getNutritionalValues() {
        return nutritionalValues;
    }

    /**
     * Imposta i valori nutrizionali dell'ingrediente
     * @param nutritionalValues valori nutrizionali dell'ingrediente
     */
    public void setNutritionalValues(Map<String, Float> nutritionalValues) {
        this.nutritionalValues = nutritionalValues;
    }

    public void addNutritionalValue(String key, Float value) {
        this.nutritionalValues.entrySet().add(new AbstractMap.SimpleEntry<>(key, value));
    }
}
