package it.cocito.chef.recipeDatabase;

public class InvalidIngredientException extends RuntimeException {
    /**
     * Indica quando vi è stato un problema durante il controllo di un ingrediente
     *
     * @param name     nome dell'ingrediente
     * @param quantity quantità dell'ingrediente
     * @param unit     unità d misura dell'ingrediente
     */
    public InvalidIngredientException(String name, String quantity, String unit) {
        super("Could not parse ingredient with name \"" + name + "\", quantity \"" + quantity + "\" and unit \"" + unit + "\".");
    }

    /**
     * Indica quando vi è stato un problema durante il controllo di un ingrediente
     * specificandone la causa
     *
     * @param name     nome dell'ingrediente
     * @param quantity quantità dell'ingrediente
     * @param unit     unità di misura dell'ingrediente
     * @param cause    causa del problema riscontrato
     */
    public InvalidIngredientException(String name, String quantity, String unit, Throwable cause) {
        super("Could not parse ingredient with name \"" + name + "\", quantity \"" + quantity + "\" and unit \"" + unit + "\".", cause);
    }
}
