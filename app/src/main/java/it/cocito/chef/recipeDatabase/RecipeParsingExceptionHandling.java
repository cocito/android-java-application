package it.cocito.chef.recipeDatabase;

/**
 * Indica le possibili azioni da compiere nel casoin cui l'ingrediente
 * che viene trovato in una ricetta non sia presente nel database degli ingredienti
 */
public enum RecipeParsingExceptionHandling {
    Discard_Details,
    Discard_Ingredient,
    Discard_Recipe,
    Throw
}
