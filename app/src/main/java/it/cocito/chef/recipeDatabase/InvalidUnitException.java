package it.cocito.chef.recipeDatabase;

public class InvalidUnitException extends Exception {
    /**
     * Indica quando vi è un problema con la quantità di un ingrediente
     *
     * @param quantity quantità dell'ingrediente
     */
    public InvalidUnitException(String quantity) {
        super("Invalid quantity \"" + quantity + "\".");
    }

    /**
     * Indica quando vi è un problema con la quantità di un ingrediente
     * specificandone la causa
     *
     * @param quantity quantità dell'ingrediente
     * @param cause    causa del problema riscontrato
     */
    public InvalidUnitException(String quantity, Throwable cause) {
        super("Invalid quantity \"" + quantity + "\".", cause);
    }
}
