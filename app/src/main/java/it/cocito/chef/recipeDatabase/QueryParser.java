package it.cocito.chef.recipeDatabase;

import java.net.MalformedURLException;

import it.cocito.chef.serverConnection.ServerConnectionFailureException;
import it.cocito.chef.serverConnection.ServerConnectionUtil;

public final class QueryParser {
    private static final String UNIT_CONVERSION_DB_URL = "http://mariolmo.altervista.org/ConversioneMisure.php";

    /**
     * Crea un'istanza QueryParser. Non dovrebbe mai essere chiamato
     */
    private QueryParser() {
        throw new AssertionError("This constructor should never be called.");
    }

    /**
     * Controlla l'unità di misura di un ingrediente
     *
     * @param unit unità di misura dell'ingrediente
     * @return restituisce l'unità di misura dell'igrediente
     * @throws InvalidUnitException             errore nell'unità di misura
     * @throws ServerConnectionFailureException fallita connessione al server
     */
    public static float parseUnit(String unit) throws InvalidUnitException, ServerConnectionFailureException {
        String response = null;

        if (unit == null) {
            throw new InvalidUnitException(unit);
        }

        try {
            response = ServerConnectionUtil.sendGetRequest(UNIT_CONVERSION_DB_URL, "q=" + unit);
        } catch (MalformedURLException e) {
            throw new InvalidUnitException(unit);
        }
        try {
            return Float.parseFloat(response);
        } catch (NumberFormatException e) {
            throw new InvalidUnitException(unit, e);
        }
    }

    /**
     * Pulisce la stringa di ricerca sostituendo i caratteri con degli spazi
     *
     * @param originalQuery stringa di ricerca
     * @param minimumLength lunghezza minima della query
     * @return restituisce la stringa di ricerca contenente solo spazi
     */
    public static String cleanQuery(String originalQuery, int minimumLength) {

        if (originalQuery == null) {
            throw new IllegalArgumentException("originalQuery must not be null.");
        }
        if (minimumLength < 1) {
            throw new IllegalArgumentException("minimumLength must be at least 1.");
        }
        //Rimpiazza i caratteri non alfanumerici (fatta eccezione per lo spazio) con spazi
        String preprocessedQuery = originalQuery.replaceAll("[^A-Za-z0-9 ]", " ");

        //Rimuovi le parole con lunghezza inferiore a minimumLength
        String[] words = preprocessedQuery.split(" ");
        String cleanQuery = "";
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (word.length() >= minimumLength) {
                cleanQuery += word;
                if (i != words.length - 1) {
                    cleanQuery += " ";
                }
            }
        }
        return cleanQuery;
    }
}
