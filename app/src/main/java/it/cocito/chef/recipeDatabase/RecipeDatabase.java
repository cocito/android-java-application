package it.cocito.chef.recipeDatabase;

import android.util.Log;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.cocito.chef.Ingredient;
import it.cocito.chef.Recipe;
import it.cocito.chef.nutritionalValues.NutritionalValuesCalculator;
import it.cocito.chef.serverConnection.InvalidQueryException;
import it.cocito.chef.serverConnection.ServerConnectionFailureException;
import it.cocito.chef.serverConnection.ServerConnectionUtil;

public final class RecipeDatabase {

    private final static int DEFAULT_MAX_RECIPES = 5;
    private final static String RECIPES_FROM_NAME_DB_URL = "http://mariolmo.altervista.org/Ricetta.php";
    private final static String RECIPES_FROM_INGREDIENTS_DB_URL = "http://mariolmo.altervista.org/RicettaDaIngredienti.php";

    /**
     * Crea un'istanza RecipeDatabase. Non dovrebbe mai essere chiamato
     */
    private RecipeDatabase() {
        throw new AssertionError("This constructor should never be called.");
    }

    /**
     * Ricerca sul server una ricetta in base al nome
     *
     * @param name                           nome della ricetta
     * @param max                            limite
     * @param recipeParsingExceptionHandling gestione degli eventuali problemi con un ingrediente
     * @return restituisce la ricetta
     * @throws ServerConnectionFailureException fallita connessione a server
     * @throws InvalidQueryException            stringa di ricerca non valida
     */
    public static List<Recipe> getRecipesFromName(String name, int max, RecipeParsingExceptionHandling recipeParsingExceptionHandling)
            throws ServerConnectionFailureException, InvalidQueryException {
        List<Map<String, Object>> recipes = new ArrayList<Map<String, Object>>();

        Log.d("RecipeDatabase", "Ricevuta query con nome \"" + name + "\".");
        try {
            recipes = (List<Map<String, Object>>) ServerConnectionUtil.getObjectResponse(RECIPES_FROM_NAME_DB_URL, recipes, "q=" + name, "limite=" + max);
        } catch (MalformedURLException e) {
            throw new InvalidQueryException(name, e);
        }
        if (recipes != null) {
            return parseRecipes(recipes, recipeParsingExceptionHandling);
        } else {
            return null;
        }
    }

    /**
     * Ricerca sul server una ricetta in base agli ingredienti che la compongono
     *
     * @param ingredients                    lista degli ingredienti
     * @param maxDistance
     * @param max                            limite
     * @param recipeParsingExceptionHandling gestione degli eventuali problemi con un ingrediente
     * @return restituisce la ricetta
     * @throws ServerConnectionFailureException fallita connessione al server
     * @throws InvalidQueryException            errore nella stringa di ricerca
     */
    public static List<Recipe> getRecipesFromIngredients(String[] ingredients, float maxDistance, int max, RecipeParsingExceptionHandling recipeParsingExceptionHandling)
            throws ServerConnectionFailureException, InvalidQueryException {
        List<Map<String, Object>> recipes = new ArrayList<Map<String, Object>>();

        Log.d("RecipeDatabase", "Ricevuta query con " + ingredients.length + " ingredienti.");

        String query = "";

        for (int i = 0; i < ingredients.length; i++) {
            query += ingredients[i];

            if (i < ingredients.length - 1) {
                query += ";";
            }
        }

        try {
            recipes = (List<Map<String, Object>>) ServerConnectionUtil.getObjectResponse(RECIPES_FROM_INGREDIENTS_DB_URL, recipes,
                    "q=" + query,
                    "limite=" + max,
                    "distanzaMax=" + maxDistance);
        } catch (MalformedURLException e) {
            throw new InvalidQueryException(query, e);
        }

        if (recipes != null) {
            return parseRecipes(recipes, recipeParsingExceptionHandling);
        } else {
            return null;
        }
    }

    /**
     * Controlla la ricetta
     *
     * @param recipes lista degli ingredienti della ricetta
     * @param recipeParsingExceptionHandling gestione degli eventuali problemi con un ingrediente
     * @return restituisce la ricetta controllata
     * @throws ServerConnectionFailureException fallita connesione al server
     */
    private static List<Recipe> parseRecipes(List<Map<String, Object>> recipes, RecipeParsingExceptionHandling recipeParsingExceptionHandling)
            throws ServerConnectionFailureException {
        List<Recipe> parsedRecipes = new ArrayList<>();

        boolean discardRecipeIfFailed = recipeParsingExceptionHandling == RecipeParsingExceptionHandling.Discard_Recipe;

        for (Map<String, Object> recipe : recipes) {
            try {
                parsedRecipes.add(parseRecipe(recipe, recipeParsingExceptionHandling));
            } catch (InvalidIngredientException e) {
                if (!discardRecipeIfFailed) {
                    throw e;
                }
            }
        }
        return parsedRecipes;
    }

    /**
     * COntrolla un ricetta
     *
     * @param map mappa degli ingredinti
     * @param recipeParsingExceptionHandling gestione degli eventuali problemi con un ingrediente
     * @return restituisce la ricetta controllata
     * @throws ServerConnectionFailureException
     */
    private static Recipe parseRecipe(Map<String, Object> map, RecipeParsingExceptionHandling recipeParsingExceptionHandling)
            throws ServerConnectionFailureException {
        Recipe recipe = new Recipe();
        recipe.setName((String) map.get("Nome"));
        recipe.setProcedure((String) map.get("Procedimento"));
        recipe.setTime(Float.parseFloat((String) map.get("Tempo")));

        List<Map<String, Object>> ingredients = (List<Map<String, Object>>) map.get("Ingredienti");

        List<Ingredient> parsedIngredients = new ArrayList<>();

        boolean discardIngredientIfFailed = recipeParsingExceptionHandling == RecipeParsingExceptionHandling.Discard_Ingredient;
        boolean discardDetailsIfFailed = recipeParsingExceptionHandling == RecipeParsingExceptionHandling.Discard_Details;

        for (Map<String, Object> ingredient : ingredients) {
            try {

                parsedIngredients.add(parseingredient(
                        (String) ingredient.get("Nome"),
                        (String) ingredient.get("Quantit"),
                        (String) ingredient.get("UnitMisura"),
                        discardDetailsIfFailed)
                );
            } catch (InvalidIngredientException e) {
                if (!discardIngredientIfFailed) {
                    throw e;
                }
            }
        }

        recipe.setIngredients(parsedIngredients);

        return recipe;
    }

    /**
     * Controlla un ingrediente
     *
     * @param name nome dell'ingrediete
     * @param quantity quntità dell'ingrediente
     * @param unit unità di misura dell'ingrediente
     * @param discardDetailsIfFailed fa si che non si consideri un ingrediente se il controllo fallisce
     * @return restituisce un ingrediente controllato
     * @throws ServerConnectionFailureException fallita connesione al server
     * @throws InvalidIngredientException dettagli dell'ingrediente non validi
     */
    private static Ingredient parseingredient(String name, String quantity, String unit, boolean discardDetailsIfFailed)
            throws ServerConnectionFailureException, InvalidIngredientException {

        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Parater \"name\" is empty.");
        }

        try {
            Ingredient ingredient = null;
            //Per quantità sconosciute senza quantità particolari si imposta quantity a 0 e visualizedQuantity a ""
            if ((quantity == null || quantity.isEmpty()) && isUnitTrivial(unit)) {
                ingredient = NutritionalValuesCalculator.getIngredient(name, 0);
                ingredient.setVisualisedQuantity("");
            }
            //Per quantità note senza unità particolari le si passa direttamente
            else if (isUnitTrivial(unit)) {
                ingredient = NutritionalValuesCalculator.getIngredient(name, Float.parseFloat(quantity.replace(",", ".")));
                String unitName = "";
                //Se l'unità è nota la si aggiunge
                if (unit != null && !unit.isEmpty()) {
                    unitName = " " + unit;
                }
                ingredient.setVisualisedQuantity(quantity + unitName);
            }
            //Per unità particolari si trasforma in g e si moltiplica per la quantità (se esiste)
            else {
                float parsedUnit = QueryParser.parseUnit(unit);
                float parsedQuantity;

                if (quantity == null || quantity.isEmpty()) {
                    parsedQuantity = 1;
                } else {
                    parsedQuantity = Float.parseFloat(quantity.replace(",", "."));
                }

                ingredient = NutritionalValuesCalculator.getIngredient(name, parsedUnit * parsedQuantity);

                ingredient.setVisualisedQuantity(getVisualisedQuantity(quantity, unit));
            }

            return ingredient;
        } catch (InvalidQueryException | InvalidUnitException e) {
            if (discardDetailsIfFailed) {
                try {
                    Ingredient ingredient = NutritionalValuesCalculator.getIngredient("Empty", 0);
                    ingredient.setName(name);
                    ingredient.setVisualisedQuantity(getVisualisedQuantity(quantity, unit));
                    return ingredient;
                } catch (InvalidQueryException f) {
                    throw new AssertionError("Could not find nutrient \"Empty\".");
                }
            } else {
                throw new InvalidIngredientException(name, quantity, unit, e);
            }
        }
    }

    /**
     * Controlla il contenuto di unità di misura
     *
     * @param unit unità di misura dell'ingrediente
     * @return restituisce l'unità di misura
     */
    private static boolean isUnitTrivial(String unit) {
        return (unit == null || unit.isEmpty() || unit.equals("g") || unit.equals("ml"));
    }

    /**
     * Prende in considerazione la quantità dell'alimento che viene visualizzata
     *
     * @param quantity quantità dell'ingrediente
     * @param unit unità di misura dell'ingrediente
     * @return quantità con relativa unità di misura
     */
    private static String getVisualisedQuantity(String quantity, String unit) {
        String visualisedQuantity = (quantity == null || quantity.isEmpty()) ? "" : quantity + " ";
        return visualisedQuantity + unit;
    }
}
