package it.cocito.chef;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.cocito.chef.nutritionalValues.NutrientRange;

public class Recipe {
    private List<Ingredient> ingredients;
    private String name, procedure;
    private float time;
    private Map<String, Float> nutritionalValues;

    public Recipe() {}

    /**
     * Crea una ricetta
     *
     * @param ingredients lista degli ingredienti necessari
     * @param name        nome della ricetta
     * @param procedure   procedimento da seguire
     * @param time        tempo richiesto per il completamento della ricetta
     */
    public Recipe(List<Ingredient> ingredients, String name, String procedure, float time) {
        this.name = name;
        this.time = time;
        this.procedure = procedure;
        this.ingredients = ingredients;
        nutritionalValues = new HashMap<>();

        calculate();
    }

    //Calcola i valori nutritivi complessivi
    public void calculate() {
        for (Ingredient ingredient : ingredients) {
            for (Map.Entry<String, Float> entry : ingredient.nutritionalValues.entrySet()) {
                String n = entry.getKey();
                float nutritionalValue = entry.getValue();
                if (nutritionalValues.keySet().contains(n)) {
                    nutritionalValues.put(n, nutritionalValues.get(n) + nutritionalValue);
                } else {
                    nutritionalValues.put(n, nutritionalValue);
                }
            }
        }
    }

    /**
     * Invia la ricetta al database
     */
    public void RecipeUp() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbref = database.getReference("https://project-dd775.firebaseio.com/");
        dbref.push().setValue(this);
    }

    /**
     * Cerca il nutriente meno salutare nell'ingrediente
     *
     * @param allowedValues  valori ammessi
     * @param relativeIntake impatto che ha il nutriente sull' unhealthyness value
     * @return
     */
    public Map.Entry<String, Float> getWorstHealthProblem(Map<String, NutrientRange> allowedValues, Float relativeIntake) {

        Map<String, Float> unhealthynessValues = new HashMap<>();

        //Per ogni nutriente calcolo l'unhealthyness value

        for (Map.Entry<String, Float> entry : nutritionalValues.entrySet()) {
            String name = entry.getKey();
            float amount = entry.getValue();
            unhealthynessValues.put(name, allowedValues.get(name).getUnhealtynessValue(amount, relativeIntake));
        }

        //Guardo quale nutriente è messo peggio (cioè ha il più alto unhealthyness value)

        Map.Entry<String, Float> maxEntry = null;

        for (Map.Entry<String, Float> entry : unhealthynessValues.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        return maxEntry;
    }

    /**
     * Ricerca tra le ricette sul database in base agli ingredienti
     *
     * @return restituisce il risultato della ricerca nel database
     */
    /*static public List<Recipe> SearchByIngredient(List<Ingredient> ingredientList) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbref = database.getReference("https://project-dd775.firebaseio.com/");
        TODO: la query va qui
        final List<Recipe> Results;
        dbref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Recipe RecipeFound;
                RecipeFound = dataSnapshot.getValue(Recipe.class);
                Results.add(RecipeFound);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return Results;
    } questa parte di codice va controllata/riscritta*/

    // GETTERS & SETTERS

    /**
     * Prende gi ingredienti per una ricetta
     *
     * @return restituisce la lista degli ingredienti
     */
    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    /**
     * Imposta la lista degli ingredienti di una ricetta
     *
     * @param ingredients lista degli ingredienti
     */
    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    /**
     * Prende il nome della ricetta
     *
     * @return restituisce il nome della ricetta
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il nome della ricetta
     *
     * @param name nome della ricetta
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Prende il procedimento da seguire per cucinare il piatto
     *
     * @return restituisce il procedimento
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * Imposta il procedimento da seguire per cucinare il piatto
     *
     * @param procedure procedimento
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    /**
     * Prende il tempo necessario per completare il piatto
     *
     * @return restituisce il tempo necessario
     */
    public float getTime() {
        return time;
    }

    /**
     * Imposta il tempo necessario per completare il piatto
     *
     * @param time restituisce il tempo necessario per completare il piatto
     */
    public void setTime(float time) {
        this.time = time;
    }

    /**
     * Prende i valori nutrizionali del piatto cucinato
     *
     * @return restituisce i valori nutrizionali del piatto
     */
    public Map<String, Float> getNutritionalValues() {
        return nutritionalValues;
    }

    /**
     * Imposta i valori nutrizionali del piatto cucinato
     *
     * @param nutritionalValues valori nutrizionali del piatto
     */
    public void setNutritionalValues(Map<String, Float> nutritionalValues) {
        this.nutritionalValues = nutritionalValues;
    }

    public void addIngredient(Ingredient i) {
        ingredients.add(i);
        calculate();
    }
}
