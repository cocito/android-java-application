package it.cocito.chef.dietElements;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.cocito.chef.Recipe;
import it.cocito.chef.utils.Time;

public class MealBlock {
    private List<Recipe> recipes;
    private String name;
    private Time time;
    private Map<String, Float> nutritionalValues;

    /**
     * Attribuisce informazioni a un pasto
     *
     * @param recipes lista dei piatti che compongono il pasto
     * @param name    nome del pasto
     * @param time    orario del pasto
     */
    public MealBlock( List<Recipe> recipes, String name, Time time) {
        this.name = name;
        this.time = time;
        this.recipes = recipes;

        calculate();
    }

    /**
     * Definisce un pasto
     *
     * @param name nome del pasto
     * @param time orario del pasto
     */
    public MealBlock(String name, Time time) {
        new MealBlock( new ArrayList<Recipe>(), name, time);
    }

    public MealBlock() {

    }

    /**
     * Calcola i valori nutritivi in base ai piatti che compongono il pasto
     */
    private void calculate() {
        // Calcolo i valori nutritivi
        for (Recipe recipe : recipes)
            for (Map.Entry<String, Float> entry : recipe.getNutritionalValues().entrySet()) {
                String name = entry.getKey();
                Float value = entry.getValue();
                if (nutritionalValues.containsKey(name)) {
                    nutritionalValues.put(name, nutritionalValues.get(name) + value);
                } else {
                    nutritionalValues.put(name, value);
                }
            }
    }

    // GETTERS & SETTERS

    /**
     * Prende il nome della ricetta
     *
     * @return restituisce il nome della ricetta
     */
    public String getName() {
        return name;
    }

    /**
     * Attribuisce un valore al nome del pasto
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Prende l'oraro della pasto
     *
     * @return restituisce l'orario del pasto
     */
    public Time getTime() {
        return time;
    }

    /**
     * Attribuisce un valore all'orario del pastoo
     *
     * @param time orario
     */
    public void setTime(Time time) {
        this.time = time;
    }

    /**
     * Prende le ricette dei piatti che compongono il pasto
     *
     * @return restituisce una ista delle ricette che compongono il pasto
     */
    public List<Recipe> getRecipes() {
        return recipes;
    }

    /**
     * Attribuisce un valore alle ricette che compongono il pasto
     *
     * @param recipes lista di ricette
     */
    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    /**
     * Prende le informazioni sui valori nutrizionali
     *
     * @return restituisce i valori utrizionali
     */
    public Map<String, Float> getNutritionalValues() {
        return nutritionalValues;
    }

    /**
     * Attribuisce un valore ai valori nutrizionali
     *
     * @param nutritionalValues valori nutrizionali dei piatti
     */
    public void setNutritionalValues(Map<String, Float> nutritionalValues) {
        this.nutritionalValues = nutritionalValues;
    }

    public void addRecipe(Recipe r) {
        recipes.add(r);
        calculate();
    }
}
