package it.cocito.chef.dietElements;

import android.util.JsonWriter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

import it.cocito.chef.Ingredient;
import it.cocito.chef.Recipe;
import it.cocito.chef.utils.Time;

public class DailySchedule {
    private String name;
    private List<MealBlock> content;
    private boolean isActive;
    private String day;
    //CONSTRUCTORS

    /**
     * Crea un'istanza DAilySchedule con valori di default
     */
    public DailySchedule() {
        /*valori di default*/
        this.name = "Schedule";
        this.isActive=false;
        this.content=null;
        this.day=null;
    }

    /**
     * Crea un'istanza DailySchedule attibuendole dei valori
     *
     * @param name     nome della lista
     * @param isActive se la lista è attiva
     * @param content  informzaioni contenute nela lista
     * @param day      giorno
     */
    public DailySchedule(String name, boolean isActive, List<MealBlock> content, String day) {
        this.name = name;
        this.isActive=isActive;
        this.content=content;
        this.day=day;
    }
    //END CONSTRUCTORS

    /**
     * Salva i dati su un file JSON
     *
     * @param filename file JSON che viene creato
     * @throws IOException segnala un errore in input o output
     */
    public void WriteToJson(OutputStream filename) throws IOException{
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(filename));
        writer.setIndent("  ");
        writer.beginObject();
        writer.name("name").value(this.getName());
        writer.name("day").value(this.getDay());
        writer.name("isActive").value(this.isActive());
        WriteBlockArray(writer, this.getContent());
        writer.endObject();
        writer.close();
    }

    /*Vedi ScheduleSample.json per un esempio di salvataggio*/

/*Utility per WriteToJson*/

    /**
     * Scrive una lista di pasti sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive i file JSON
     * @param blocks lista dei pasti da srivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteBlockArray(JsonWriter writer, List<MealBlock> blocks) throws IOException {
        writer.name("blocks").beginArray();
        if(blocks != null){
            for (MealBlock block: blocks) {
                WriteBlock(writer, block);
            }
        }
        writer.endArray();
    }

    /**
     * Scrive un pasto sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param block  pasto da scrivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteBlock(JsonWriter writer, MealBlock block) throws IOException {
        writer.beginObject();
        WriteRecipeArray(writer, block.getRecipes());
        writer.name("name").value(block.getName());
        WriteTime(writer, block.getTime());
        writer.endObject();
    }

    /**
     * Scrive una lista delle ricette che compongono un pasto sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param recipes lista delle ricette
     * @throws IOException segnala un errore in input o output
     */
    private void WriteRecipeArray(JsonWriter writer, List<Recipe> recipes) throws IOException{
        writer.name("recipes").beginArray();
        for (Recipe recipe: recipes) {
            WriteRecipe(writer, recipe);
        }
        writer.endArray();
    }

    /**
     * Scrive una ricetta sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che srive sul file JSON
     * @param recipe ricetta da scrivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteRecipe(JsonWriter writer, Recipe recipe) throws IOException {
        writer.beginObject();
        WriteIngredientArray(writer, recipe.getIngredients());
        writer.name("name").value(recipe.getName());
        writer.name("procedure").value(recipe.getProcedure());
        writer.name("time").value(recipe.getTime());
        writer.endObject();
    }

    /**
     * Srive una lista degli ingredienti che compongono una ricetta sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param ingredients lista degli ingredienti
     * @throws IOException segnala un errore in input o output
     */
    private void WriteIngredientArray(JsonWriter writer, List<Ingredient> ingredients) throws IOException {
        writer.name("ingredients").beginArray();
        for (Ingredient ingredient: ingredients) {
            WriteIngredient(writer, ingredient);
        }
        writer.endArray();
    }

    /**
     * Scrive un ingrediente sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param ingredient ingrediente da scrivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteIngredient(JsonWriter writer, Ingredient ingredient) throws IOException {
        writer.beginObject();
        writer.name("name").value(ingredient.getName());
        writer.name("quantity").value(ingredient.getQuantity());
        WriteNutritionalValues(writer, ingredient.getNutritionalValues());
        writer.endObject();
    }

    /**
     * Scrive l'orario del pasto sul file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param time orario da scrivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteTime(JsonWriter writer, Time time) throws IOException {
        writer.name("time").beginObject();
        writer.name("hour").value(time.getHour());
        writer.name("minutes").value(time.getMinutes());
        writer.name("seconds").value(time.getSeconds());
        writer.endObject();
    }

    /**
     * Srive i valori nutrizionali su uun file JSON
     *
     * @param writer istanza di tipo JsonWriter che scrive sul file JSON
     * @param nutritionalValues valori nutrizionali da scrivere
     * @throws IOException segnala un errore in input o output
     */
    private void WriteNutritionalValues(JsonWriter writer, Map<String, Float> nutritionalValues) throws IOException {
        writer.name("nutritionalValues").beginArray();
        for (Map.Entry<String, Float> value: nutritionalValues.entrySet()) {
            writer.beginObject();
            writer.name("key").value(value.getKey());
            writer.name("value").value(value.getValue());
            writer.endObject();
        }
        writer.endArray();
    }

/*Fine utility per WriteToJson*/

    public void ReadFromJson(FileInputStream is) throws IOException {
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        final String json = new String(buffer);
        final JsonObject root = new JsonParser().parse(json).getAsJsonObject();
        this.setName(root.get("name").getAsString());
        this.setDay(root.get("day").getAsString());
        this.setActive(root.get("isActive").getAsBoolean());
        final JsonArray blocks = root.getAsJsonArray("blocks");
        for (JsonElement blockElem : blocks) {
            final JsonObject block = blockElem.getAsJsonObject();
            final MealBlock mb = new MealBlock();

            mb.setName(block.get("name").getAsString());
            final JsonObject t = block.getAsJsonObject("time");
            mb.setTime(new Time(
                    t.get("hour").getAsInt(),
                    t.get("minutes").getAsInt(),
                    t.get("seconds").getAsInt()
            ));
            final JsonArray recipes = block.getAsJsonArray("recipes");
            for (JsonElement recipElem : recipes) {
                final JsonObject recipe = recipElem.getAsJsonObject();
                final Recipe r = new Recipe();

                r.setName(recipe.get("name").getAsString());
                r.setProcedure(recipe.get("procedure").getAsString());
                r.setTime(recipe.get("time").getAsFloat());

                final JsonArray ingredients = recipe.getAsJsonArray("ingredients");
                for (JsonElement ingElem : ingredients) {
                    final JsonObject ing = ingElem.getAsJsonObject();
                    final Ingredient i = new Ingredient();

                    i.setName(ing.get("name").getAsString());
                    i.setQuantity(ing.get("quantity").getAsFloat());
                    final JsonArray nutVals = ing.getAsJsonArray("nutritionalValues");
                    for (JsonElement nutValElem : nutVals) {
                        final JsonObject nutVal = nutValElem.getAsJsonObject();
                        String k = nutVal.get("key").getAsString();
                        Float v = nutVal.get("value").getAsFloat();
                        i.addNutritionalValue(k, v);
                    }

                    r.addIngredient(i);
                }

                mb.addRecipe(r);
            }

            content.add(mb);
        }
    }
/*Fine Utility per ReadFromJson*/

//GETTERS AND SETTERS
    //GETTERS

    /**
     * Prende una lista di pasti e ne restituisce il contenuto
     *
     * @return restiutisce il contenuto delle informazione nella lista di pasti
     */
    public List<MealBlock> getContent() {
        return content;
    }

    //SETTERS

    /**
     * Attribuisce un valore alla lista di pasti
     *
     * @param content lasta in cui viene salvata la lista di pasti
     */
    public void setContent(List<MealBlock> content) {
        this.content = content;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
    //END GETTERS

    /**
     * Prende il giorno in cui vine stilata la lista di pasti
     *
     * @return restituisce la data
     */
    public String getDay() {
        return day;
    }

    /**
     * Attribuisce un valore alla data del giorno in cui viene stilata la lista di pasti
     *
     * @param day data
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     * Prende il nome della lista di pasti
     *
     * @return restituisce il nome della lista
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il nome della lista dei pasti
     *
     * @param name nome della lista
     */
    public void setName(String name) {
        this.name = name;
    }
    //END SETTERS
//END GETTERS AND SETTERS
}