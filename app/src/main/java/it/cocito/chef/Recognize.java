package it.cocito.chef;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.Locale;

public class Recognize {
    protected final SearchView searchView;
    /*
        getState: restituisce stato operazioni -> 0: attesa, 1: lavorando, 2: finito, 3: errore
        getResult: restituisce parole (ArrayList<String>)
        getNewResult: restituisce parole non ancore lette
        Recognize: cotruttore, parametro Context per passarlo a SpeechRecognizer
        startListening: inizio lavoro
        errorText: comunicato all'utente in caso di errore

        state: stato operazioni
        result: tutte parole
        newResult: parone non ancora lette
        intent: impostazioni per SpeechRecognize
        sp: SpeechRecognizer
        tts: Text-To_specch
    */
    //result: parole riconosciute
    //protected ArrayList<String> result = new ArrayList<>();
    //protected ArrayList<String> newResult = new ArrayList<>();
    //comunicato all'utente in caso di errore (CharSequence per tts.speak)
    public CharSequence errorText;
    //intent: impostazioni per il riconoscimento
    protected Intent intent;
    //sp: SpeechRecognizer
    protected SpeechRecognizer sp;
    //stato operazione: 0: attesa, 1: lavorando, 2: finito, 3: errore
    protected RecognizeState state = RecognizeState.Waiting;
    //text-to-speech: comunicare errori all'utente
    protected TextToSpeech tts;

    public Recognize(Context context, CharSequence errorT, SearchView searchView) {
        //result = new ArrayList<>();
        //newResult = new ArrayList<>();

        this.searchView = searchView;

        errorText = (errorT == null) ? "Non ho capito" : errorT;

        intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "it_IT");
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 5000);
        //intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "it.cocito.chef");
        sp = SpeechRecognizer.createSpeechRecognizer(context);
        sp.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                Log.d("SpeechRecognizer", "readyForSpeech");
            }

            @Override
            public void onBeginningOfSpeech() {
                Log.d("SpeechRecognizer", "beginning");
            }

            @Override
            public void onRmsChanged(float rmsdB) {
                //Log.d("SpeechRecognizer","RmsChanged");
            }

            @Override
            public void onBufferReceived(byte[] buffer) {
                Log.d("SpeechRecognizer", "BufferReceived");
            }

            @Override
            public void onEndOfSpeech() {

                Log.d("SpeechRecognizer", "End");
            }

            @Override
            public void onError(int error) {
                Log.d("SpeechRecognizer", "Error " + error);
                if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {    //speech timeout
                    state = RecognizeState.Waiting;
                } else if (error == SpeechRecognizer.ERROR_NO_MATCH) {   //no match
                    if(!tts.isSpeaking()) {
                        Log.d("tts",String.format("%d",tts.speak(errorText, TextToSpeech.QUEUE_FLUSH, null, null)));
                    }
                } else if (error != SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
                    state = RecognizeState.Error;
                }
            }

            @Override
            public void onResults(Bundle results) {
                state = RecognizeState.Finished;
                Log.d("SpeechRecognizer", "Result");
                ArrayList<String> words = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);  //creare una classe con array di string e aggiungere risultato
                setSearchQuery(words.get(0));
            }

            @Override
            public void onPartialResults(Bundle partialResults) {
                Log.d("SpeechRecognizer", "PartialResult");
                String[] tmp = partialResults.getStringArray(SpeechRecognizer.RESULTS_RECOGNITION);
                //newResult.clear();
                //newResult.addAll(Arrays.asList(tmp));
            }

            @Override
            public void onEvent(int eventType, Bundle params) {
                Log.d("SpeechRecognizer", "Event");
            }
        });

        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                Log.d("tts", String.format("OnInit %d", status));
                if (status == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.getDefault());
                }
            }
        });
    }

    public RecognizeState getState() {
        return this.state;
    }

    /*public ArrayList<String> getResult() {
        if (state != RecognizeState.Finished) {    //lavoro non concluso -> restituisco lista vuota
            return null;
        }
        state = RecognizeState.Waiting;  //in attesa -> ha già restituito result
        return this.result;
    }

    public ArrayList<String> getNewResult() {
        if (state == RecognizeState.Waiting) {
            //return new ArrayList<String>();
            return null;
        }
        ArrayList<String> tmp =(ArrayList<String>)newResult.clone();
        newResult.clear();
        return tmp;
    }*/

    public void startListening(){
        if (state != RecognizeState.Executing) {   //se non sta lavorando
            state = RecognizeState.Executing;
            Log.d("Recognize", "startListening");
            sp.startListening(intent);
        }
    }

    public void stopListening(){
        if (state == RecognizeState.Executing) {
            state = RecognizeState.Finished;
            sp.stopListening();
            Log.d("Recognize", "stopLitening");
        }
        if (state == RecognizeState.Error /*|| result.size() == 0*/) {
            state = RecognizeState.Waiting;
        }
    }

    private void setSearchQuery(String query) {
        String presentQuery = searchView.getQuery().toString();
        if (presentQuery.isEmpty()) {
            searchView.setQuery(query, true);
        } else {
            searchView.setQuery(presentQuery + ";" + query, true);
        }
    }

    public enum RecognizeState {
        Waiting,
        Executing,
        Finished,
        Error
    }

}
