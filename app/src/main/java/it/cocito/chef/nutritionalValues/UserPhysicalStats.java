package it.cocito.chef.nutritionalValues;

public class UserPhysicalStats {
    public Gender gender;
    public int age;
    public float height;
    public float weight;
    public ActivityLevel activityLevel;

    /**
     * Informazioni relative all'utente
     *
     * @param gender Sesso
     * @param age Età
     * @param height Altezza
     * @param weight Peso
     * @param activityLevel Livello di attività fisica
     */
    public UserPhysicalStats(Gender gender, int age, float height, float weight, ActivityLevel activityLevel) {
        this.gender = gender;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.activityLevel = activityLevel;
    }

    public enum Gender {
        Unknown,
        Male,
        Female,
        Other
    }

    public enum ActivityLevel {
        Unknown,//Sconosciuto
        Sedentary,//Poco o niente esercizio
        Light,//1-3 giorni/settimana
        Moderate,//3-5 giorni/settimana
        Heavy,//6-7 giorni/settimana
        Very_Heavy//2 volte al giorno, esercizi molto intensi
    }
}
