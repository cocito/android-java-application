package it.cocito.chef.nutritionalValues;

public class NutrientRange {
    public float min;
    public float max;
    private float idealValue;
    private boolean idealValueIsSet = false;

    private NutrientRange() {

    }

    /**
     * Definisce un intervallo entro il quale sarebbe opportuno inserire
     * la dose giornaliera del nutriente
     *
     * @param min Dose minima del nutriente che si può assumere ogni giorno
     * @param max Dose massima del nutriente che si può assumere ogni giorno
     */
    public NutrientRange(float min, float max) {
        if (max <= min || Float.isInfinite(min) || Float.isNaN(min) || Float.isInfinite(max) || Float.isNaN(max)) {
            throw new InvalidNutrientRangeException(min, max);
        }
        this.min = min;
        this.max = max;
    }

    /**
     * Definisce un intervallo entro il quale sarebbe opportuno inserire
     * la dose giornaliera del nutriente
     *
     * @param min Dose minima del nutriente che si può assumere ogni giorno
     * @param max Dose massima del nutriente che si può assumere ogni giorno
     * @param idealValue Dose ideale del nutriente da assumere ogni giorno
     */
    public NutrientRange(float min, float max, float idealValue) {
        this(min, max);
        setIdealValue(idealValue);
    }

    /**
     * Definisce un intervallo entro il quale sarebbe opportuno inserire
     * la dose giornaliera del nutriente considerando possibile un eccesso o un difetto
     * trascurabile
     *
     * @param idealValue Dose ideale del nutriente da assumere ogni giorno
     * @param allowedRelativeError Errore ammesso
     * @return Intervallo entro il quale sarebbe opportuno inserire la dose giornaliera del nutriente
     */
    public static NutrientRange fromExpected(float idealValue, float allowedRelativeError) {
        return new NutrientRange(idealValue * (1 - allowedRelativeError), idealValue * (1 + allowedRelativeError), idealValue);
    }

    /**
     * Calcola la gravità con cui una quantità è fuori dai
     * parametri stabiliti
     *
     * @param amount Quantità
     * @param relativeIntake Quanto contribuisce (0.0-1.0) al fabbisogno giornaliero
     * @return La gravità (0 se è nei parametri stabiliti)
     */
    public float getUnhealtynessValue(float amount, float relativeIntake) {
        float actualMin = min * relativeIntake;
        float actualMax = max * relativeIntake;
        if (amount < actualMin) {
            return (float) Math.pow((amount - actualMin) / (actualMax - actualMin), 2);
        }
        if (amount > actualMax) {
            return (float) Math.pow((amount - actualMax) / (actualMax - actualMin), 2);
        }
        return 0;
    }

    /**
     * Calcola il valore ideale
     *
     * @return Il valore ideale (se non è stato specificato ritorna la media di minimo e massimo)
     */
    public float getIdealValue() {
        if (idealValueIsSet) {
            return idealValue;
        }
        return (max + min) / 2;
    }

    /**
     *Imposta la dose ideale del nutriente da assumere ogni giorno
     *
     * @param idealValue Dose ideale del nutriente da assumere ogni giorno
     */
    private void setIdealValue(float idealValue) {
        this.idealValue = idealValue;
        idealValueIsSet = true;
    }
}
