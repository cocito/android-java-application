package it.cocito.chef.nutritionalValues;


public class InvalidNutrientRangeException extends RuntimeException {

    /**
     * Indica quando un intervallo tra valore minimo e massimo
     * del nutriente non è ammesso
     *
     * @param min Dose minima del nutriente che si può assumere ogni giorno
     * @param max Dose massima del nutriente che si può assumere ogni giorno
     */
    public InvalidNutrientRangeException(float min, float max) {
        super("The range \"" + min + "\";\"" + max + "\" is not allowed. Please use finite bounds with a nonzero positive difference.");
    }
}
