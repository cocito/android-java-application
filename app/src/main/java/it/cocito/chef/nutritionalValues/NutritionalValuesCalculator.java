package it.cocito.chef.nutritionalValues;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import it.cocito.chef.Ingredient;
import it.cocito.chef.serverConnection.InvalidQueryException;
import it.cocito.chef.serverConnection.ServerConnectionFailureException;
import it.cocito.chef.serverConnection.ServerConnectionUtil;

public final class NutritionalValuesCalculator {

    public static final Map<String, String> DB_TRANSLATIONS;
    private static final String NUTRITIONAL_VALUES_DB_URL = "http://mariolmo.altervista.org/FuzzySearch.php";
    private static final String DAILY_REQUIREMENTS_DB_URL = "http://mariolmo.altervista.org/FabbisognoGiornaliero.php";
    private static final float DEFAULT_ALLOWED_ERROR = 0.05f;

    static {
        DB_TRANSLATIONS = new HashMap<>();
        DB_TRANSLATIONS.put("Name", "Nome");
        DB_TRANSLATIONS.put("Energy value", "Valore_energetico");
        DB_TRANSLATIONS.put("Carbohydrates", "Carboidrati");
        DB_TRANSLATIONS.put("Proteins", "Proteine");
        DB_TRANSLATIONS.put("Sugars", "Zuccheri");
        DB_TRANSLATIONS.put("Fibers", "Fibre");
        DB_TRANSLATIONS.put("Total fats", "Grassi_totali");
        DB_TRANSLATIONS.put("Saturated fatty acids", "Acidi_grassi_saturi");
        DB_TRANSLATIONS.put("Polyunsaturated fatty acids", "Acidi_grassi_polinsaturi");
        DB_TRANSLATIONS.put("Cholesterol", "Colesterolo");
    }

    /**
     * Crea un'istanza di NutritionalValuesCalculator. Non dovrebbe mai essere chiamato
     */
    private NutritionalValuesCalculator() {
        throw new AssertionError("This constructor should never be called.");
    }

    /**
     * Cerca sul server un ingrediente e ne salva le informazioni
     *
     * @param query Ricerca effettuata
     * @param quantity Quantità
     * @return Ingrediente con relativo nome, quota energetica e valori nutritivi
     * @throws ServerConnectionFailureException Fallita la connessione al server
     * @throws InvalidQueryException Ricerca non valida
     */
    public static Ingredient getIngredient(String query, float quantity)
            throws ServerConnectionFailureException, InvalidQueryException {

        if (query == null) {
            throw new InvalidQueryException(null);
        }

        Map<String, Object> responseMap = null;
        try {
            responseMap = ServerConnectionUtil.getMapResponse(NUTRITIONAL_VALUES_DB_URL, "q=" + query);
        } catch (MalformedURLException e) {
            throw new InvalidQueryException(query, e);
        }

        if (responseMap == null) {
            throw new InvalidQueryException(query);
        }

        Map<String, Float> resultMap = new HashMap<>();

        for (Map.Entry<String, Object> entry : responseMap.entrySet()) {
            //Crea la mappa dei valori nutrizionali (le quantità sono espresse per 100g/ml)
            if (!entry.getKey().equals(DB_TRANSLATIONS.get("Name"))) {
                resultMap.put(entry.getKey(), Float.parseFloat(entry.getValue().toString()) * quantity / 100);
            }
        }

        String name = (String) responseMap.get(DB_TRANSLATIONS.get("Name"));

        return new Ingredient(name, quantity, resultMap);
    }

    /**
     * Usa l'equazione di Harris-Benedict (versione rielaborata di Mifflin e St Jeor)
     * per calcolare il fabbisogno giornaliero di calorie (precisione del 95%)
     *
     * @param physicalStats Le statistiche dell'utente
     * @return Il fabbisogno (in kcal), approssimato all'unità
     */
    public static NutrientRange getDailyKcalRequirement(UserPhysicalStats physicalStats) {
        float baseMetabolism;
        if (physicalStats.gender == UserPhysicalStats.Gender.Female) {
            baseMetabolism = 10 * physicalStats.weight + 6.25f * physicalStats.height - 5 * physicalStats.age - 161;
        } else {
            baseMetabolism = 10 * physicalStats.weight + 6.25f * physicalStats.height - 5 * physicalStats.age + 5;
        }

        float actualMetabolism = 0;

        switch (physicalStats.activityLevel) {
            default://Unknown e Sedentary
                actualMetabolism = baseMetabolism * 1.2f;
                break;
            case Light:
                actualMetabolism = baseMetabolism * 1.375f;
                break;
            case Moderate:
                actualMetabolism = baseMetabolism * 1.55f;
                break;
            case Heavy:
                actualMetabolism = baseMetabolism * 1.725f;
                break;
            case Very_Heavy:
                actualMetabolism = baseMetabolism * 1.9f;
                break;
        }

        return NutrientRange.fromExpected(Math.round(actualMetabolism), 0.05f);
    }

    /**
     * Salva il fabbisogno giornaliero di ogni nutriente letto dal server
     * o calcolato in base a informazioni dell'utente
     *
     * @param physicalStats Informazioni riguardo all'utente (sesso, età, altezza, peso, livello di attività)
     * @param kcalRequirement Fabbisogno energetico
     * @return Tabella contenente il fabbisogno giornaliero per ogni nutriente
     * @throws ServerConnectionFailureException Fallita connessione al server
     */
    public static Map<String, NutrientRange> getNutritionalRequirements(UserPhysicalStats physicalStats, NutrientRange kcalRequirement)
            throws ServerConnectionFailureException {

        //Ottieni vitamine e minerali dal database

        String ageParameter = "et=" + Integer.toString(physicalStats.age);
        String genderParameter = "sesso=" + (physicalStats.gender == UserPhysicalStats.Gender.Female ? "F" : "M");

        Map<String, Object> responseMap;

        try {
            responseMap = ServerConnectionUtil.getMapResponse(DAILY_REQUIREMENTS_DB_URL, ageParameter, genderParameter);
        } catch (MalformedURLException e) {
            throw new AssertionError("Couldn't format correctly the query.");
        }

        Map<String, NutrientRange> nutrientMap = new HashMap<>();

        for (Map.Entry<String, Object> entry : responseMap.entrySet()) {
            float expectedValue = Float.parseFloat((String) entry.getValue());
            nutrientMap.put(entry.getKey(), NutrientRange.fromExpected(expectedValue, DEFAULT_ALLOWED_ERROR));
        }

        //Calcola gli altri nutrienti
        float idealKcalRequirement = kcalRequirement.getIdealValue();

        nutrientMap.put(DB_TRANSLATIONS.get("Energy value"), kcalRequirement);
        nutrientMap.put(DB_TRANSLATIONS.get("Carbohydrates"), getCarbohydrates(idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Proteins"), getProteins(physicalStats.age, physicalStats.weight, physicalStats.gender));
        nutrientMap.put(DB_TRANSLATIONS.get("Sugars"), getSugars(idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Fibers"), getFibers(physicalStats.age, idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Total fats"), getTotalFats(physicalStats.age, idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Saturated fatty acids"), getTotalFats(physicalStats.age, idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Polyunsaturated fatty acids"), getPolyunsaturatedFattyAcids(idealKcalRequirement));
        nutrientMap.put(DB_TRANSLATIONS.get("Cholesterol"), getCholesterol(physicalStats.age));

        return nutrientMap;
    }

    /**
     * Calcola un intervallo entro il quale inserire la dose
     * giornaliera di carboidrati da assumere
     *
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di carboidrati
     */
    private static NutrientRange getCarbohydrates(float kcalRequirement) {
        //1 g di carboidrati = 4 kcal. Tra il 45% e il 60% del fabbisogno energetico
        float min = kcalRequirement * 0.45f / 4;
        float max = kcalRequirement * 0.6f / 4;
        return new NutrientRange(min, max);
    }

    /**
     * Calcola un intervallo entro il quale inserire la dose
     * giornaliera di zuccheri da assumere
     *
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di zuccheri
     */
    private static NutrientRange getSugars(float kcalRequirement) {
        //1 g di zuccheri = 4 kcal. Tra lo 0% e il 15% del fabbisogno energetico
        float max = kcalRequirement * 0.15f / 4;
        return new NutrientRange(0, max);
    }

    /**
     * Calcola in base all'età dell'utente un intervallo entro il quale
     * inserire la dose giornaliera di fibre da assumere
     *
     * @param age Età dell'utente
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di fibre
     */
    private static NutrientRange getFibers(int age, float kcalRequirement) {
        if (age < 18) {
            //Per i minori 8.4 g ogni 1000 kcal
            return NutrientRange.fromExpected(kcalRequirement / 1000 * 8.4f, DEFAULT_ALLOWED_ERROR);
        } else {
            //Per gli adulti tra 12.6 g e 16.7 g ogni 1000 kcal, ma sempre almeno 25 g
            float min = Math.max(kcalRequirement / 1000 * 12.6f, 25);
            float max = Math.max(kcalRequirement / 1000 * 16.7f, 25);
            return new NutrientRange(min, max);
        }
    }

    /**
     * Calcola in base all'età dell'utente un intervallo entro il quale inserire la dose
     * giornaliera di grassi da assumere
     *
     * @param age Età dell'utente
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di grassi (saturi ed insaturi)
     */
    private static NutrientRange getTotalFats(int age, float kcalRequirement) {
        //1 g di grassi = 9 kcal
        if (age < 4) {
            //Minori di 4 anni: tra 35% e 40% del fabbisogno energetico
            float min = kcalRequirement * 0.35f / 9;
            float max = kcalRequirement * 0.4f / 9;
            return new NutrientRange(min, max);
        } else {
            //4 anni o più: tra 20% e 35% del fabbisogno energetico
            float min = kcalRequirement * 0.2f / 9;
            float max = kcalRequirement * 0.35f / 9;
            return new NutrientRange(min, max);
        }
    }

    /**
     * Calcola un intervallo entro il quale inserire la dose
     * giornaliera di acidi grassi saturi da assumere
     *
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di acidi grassi saturi
     */
    private static NutrientRange getSaturatedFattyAcids(float kcalRequirement) {
        //1 g di grassi = 9 kcal. Al massimo 15% del fabbisogno energetico
        float max = kcalRequirement * 0.15f / 9;
        return new NutrientRange(0, max);
    }

    /**
     * Calcola un intervallo entro il quale inserire la dose
     * giornaliera di acidi grassi polinsaturi da assumere
     *
     * @param kcalRequirement Fabbisogno energetico
     * @return Intervallo entro il quale inserire la dose giornaliera di acidi grassi polinsaturi
     */
    private static NutrientRange getPolyunsaturatedFattyAcids(float kcalRequirement) {
        //1 g di grassi = 9 kcal. Tra 5% e 15% del fabbisogno energetico
        float min = kcalRequirement * 0.05f / 9;
        float max = kcalRequirement * 0.15f / 9;
        return new NutrientRange(min, max);
    }

    /**
     *Calcola in base all'età dell'utente un intervallo entro
     * il quale inserire la dose giornaliera di colesterolo da assumere
     *
     * @param age Età dell'utente
     * @return Intervallo entro il quale inserire la dose giornaliera di colesterolo
     */
    private static NutrientRange getCholesterol(int age) {
        if (age < 18) {
            //Limitarlo al minimo nei minori
            return new NutrientRange(0, 0.01f);
        } else {
            //Meno di 300 mg negli adulti
            return new NutrientRange(0, 0.3f);
        }
    }

    /**
     * Calcola in base all'età, al peso e al sesso dell'utente un intervallo entro
     * il quale inserire la dose giornaliera di proteine da assumere
     *
     * @param age Età dell'utente
     * @param weight Peso dell'utente
     * @param gender Sesso dell'utente
     * @return Intervallo entro il quale inserire la dose giornaliera di proteine
     */
    private static NutrientRange getProteins(int age, float weight, UserPhysicalStats.Gender gender) {
        float gramsPerKilogram = Float.NaN;
        if (age <= 3) {
            gramsPerKilogram = 1;
        } else if (age <= 6) {
            gramsPerKilogram = 0.94f;
        } else if (age <= 10) {
            gramsPerKilogram = 0.99f;
        } else if (age <= 14) {
            if (gender == UserPhysicalStats.Gender.Female) {
                gramsPerKilogram = 0.95f;
            } else {
                gramsPerKilogram = 0.97f;
            }
        } else if (age <= 17) {
            if (gender == UserPhysicalStats.Gender.Female) {
                gramsPerKilogram = 0.9f;
            } else {
                gramsPerKilogram = 0.93f;
            }
        } else if (age <= 59) {
            gramsPerKilogram = 0.9f;
        } else {
            gramsPerKilogram = 1.1f;
        }

        return NutrientRange.fromExpected(weight * gramsPerKilogram, DEFAULT_ALLOWED_ERROR);
    }

}
