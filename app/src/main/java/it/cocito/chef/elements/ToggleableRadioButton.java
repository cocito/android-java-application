package it.cocito.chef.elements;

import android.content.Context;
import android.util.AttributeSet;

public class ToggleableRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    public ToggleableRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ToggleableRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToggleableRadioButton(Context context) {
        super(context);
    }

    @Override
    public void toggle() {
        if (isChecked())
            setChecked(false);
        else
            setChecked(true);
    }
}
