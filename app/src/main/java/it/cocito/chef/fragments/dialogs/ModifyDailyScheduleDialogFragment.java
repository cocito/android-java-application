package it.cocito.chef.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import it.cocito.chef.R;
import it.cocito.chef.dietElements.DailySchedule;
import it.cocito.chef.elements.ToggleableRadioButton;
import it.cocito.chef.fragments.DietFragment;

public class ModifyDailyScheduleDialogFragment extends DialogFragment {

    public static final int ADD = 0;
    public static final int MOD = 1;

    private boolean cancelled = false;
    private String scheduleName = "";
    private String days = "";
    private DietFragment fragment;
    private int state = ADD;
    private int scheduleID;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.diet_modify_dialog, null);

        if (state == MOD) {
            ((EditText) view.findViewById(R.id.create_schedule_name)).setText(scheduleName);
            if (getSelectedDay("Mon"))
                ((ToggleableRadioButton) view.findViewById(R.id.mon_radio)).setChecked(true);
            if (getSelectedDay("Tue"))
                ((ToggleableRadioButton) view.findViewById(R.id.tues_radio)).setChecked(true);
            if (getSelectedDay("Wed"))
                ((ToggleableRadioButton) view.findViewById(R.id.wed_radio)).setChecked(true);
            if (getSelectedDay("Thu"))
                ((ToggleableRadioButton) view.findViewById(R.id.thu_radio)).setChecked(true);
            if (getSelectedDay("Fri"))
                ((ToggleableRadioButton) view.findViewById(R.id.fri_radio)).setChecked(true);
            if (getSelectedDay("Sat"))
                ((ToggleableRadioButton) view.findViewById(R.id.sat_radio)).setChecked(true);
            if (getSelectedDay("Sun"))
                ((ToggleableRadioButton) view.findViewById(R.id.sun_radio)).setChecked(true);
        }

        builder.setTitle(state == ADD ? "Add a new diet's daily schedule" : "Modifying " + scheduleName)
                .setView(view)
                .setPositiveButton(state == ADD ? "Add" : "Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ModifyDailyScheduleDialogFragment.this.cancelled = false;
                        ModifyDailyScheduleDialogFragment.this.scheduleName = ((EditText) getDialog().findViewById(R.id.create_schedule_name)).getText().toString();
                        ModifyDailyScheduleDialogFragment.this.days = getSelectedDays();
                        if (state == ADD)
                            fragment.addItem(new DailySchedule(scheduleName, true, null, days));
                        else
                            fragment.updateItem(scheduleID, new DailySchedule(scheduleName, true, null, days));
                        dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ModifyDailyScheduleDialogFragment.this.cancelled = true;
                        ModifyDailyScheduleDialogFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }

    public String getSelectedDays() {
        boolean[] selectedDays = new boolean[7];
        String[] days = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        selectedDays[0] = ((ToggleableRadioButton) getDialog().findViewById(R.id.mon_radio)).isChecked();
        selectedDays[1] = ((ToggleableRadioButton) getDialog().findViewById(R.id.tues_radio)).isChecked();
        selectedDays[2] = ((ToggleableRadioButton) getDialog().findViewById(R.id.wed_radio)).isChecked();
        selectedDays[3] = ((ToggleableRadioButton) getDialog().findViewById(R.id.thu_radio)).isChecked();
        selectedDays[4] = ((ToggleableRadioButton) getDialog().findViewById(R.id.fri_radio)).isChecked();
        selectedDays[5] = ((ToggleableRadioButton) getDialog().findViewById(R.id.sat_radio)).isChecked();
        selectedDays[6] = ((ToggleableRadioButton) getDialog().findViewById(R.id.sun_radio)).isChecked();

        String msg = "";

        for (int i = 0; i < selectedDays.length; i++)
            if (selectedDays[i]) msg = msg.concat(days[i] + "|");

        if (msg.length() > 0)
            msg = msg.substring(0, msg.length() - 1);

        return msg;
    }

    public boolean getSelectedDay(String s) {
        return getDays().contains(s);
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public String getScheduleName() {
        return this.scheduleName;
    }

    public void setScheduleName(String s) {
        this.scheduleName = s;
    }

    public String getDays() {
        return this.days;
    }

    public void setDays(String s) {
        this.days = s;
    }

    public void setFragment(DietFragment fragment) {
        this.fragment = fragment;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

}
