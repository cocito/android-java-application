package it.cocito.chef.fragments.adapter;

import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import it.cocito.chef.R;
import it.cocito.chef.Recipe;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> {

    //TODO: Eccezioni (quando non ci sono ricette), gli altri elementi

    private static final String TAG = "RecipeAdapter";
    private static final int MAX_INGREDIENTS_PER_RECIPE = 8;

    private static Random r = new Random();

    private boolean discreteMode;

    private ArrayList<Recipe> dataSet = new ArrayList<Recipe>();

    public RecipeAdapter(ArrayList<Recipe> dataSet, boolean discreteMode) {
        this.dataSet = dataSet;
        this.discreteMode = discreteMode;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup vg, int type) {
        View v = LayoutInflater.from(vg.getContext()).inflate(R.layout.recipe_item, vg, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

                RecipeAdapter.ViewHolder vh = (ViewHolder) v.getTag();

                builder.setMessage(vh.procedure)
                        .setTitle(vh.getRecipeNameText().getText());

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        RecipeAdapter.ViewHolder vh = new ViewHolder(v);
        v.setTag(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, final int position) {

        Log.d(TAG, "Element " + position + " set.");
        Recipe recipe = dataSet.get(position);
        vh.getRecipeNameText().setText(recipe.getName());
        vh.getDuration().setText(String.format(Locale.getDefault(), "%d", (int) Math.floor(recipe.getTime())) + " minuti");
        vh.procedure = recipe.getProcedure();

        if (discreteMode) {
            vh.getIngredients().setVisibility(View.GONE);
            vh.getNutrients().setVisibility(View.GONE);
            vh.getRating().setVisibility(View.GONE);
        } else {
            String ingredients = "Ingredienti:\n";

            for (int i = 0; i < MAX_INGREDIENTS_PER_RECIPE && i < recipe.getIngredients().size(); i++) {
                ingredients += recipe.getIngredients().get(i).getName();
                if (i < recipe.getIngredients().size() - 1) {
                    ingredients += "\n";
                }
            }
            vh.getIngredients().setText(ingredients);

            String nutrients = "Valori nutrizionali:\n";

            nutrients += "Valore energetico: " + String.format(Locale.getDefault(), "%f", r.nextFloat() * 300).substring(0, 4) + " kcal\n";
            nutrients += "Proteine: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Carboidrati: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Grassi: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Vitamina A: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Vitamina C: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Vitamina D: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            nutrients += "Ferro: " + String.format(Locale.getDefault(), "%f", r.nextFloat()).substring(0, 5) + " g\n";
            vh.getNutrients().setText(nutrients);

            vh.getRating().setRating(3 + r.nextInt(3));
            String diff = "Facile";
            switch (r.nextInt(3)) {
                case 1:
                    diff = "Normale";
                    break;
                case 2:
                    diff = "Difficile";
                    break;
            }
            vh.getDifficulty().setText(diff);

        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void addItem(Recipe recipe) {
        dataSet.add(getItemCount(), recipe);
        notifyItemInserted(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView recipeName;
        private final TextView duration;
        private final TextView ingredients;
        private final RatingBar rating;
        private final TextView nutrients;
        private final TextView difficulty;
        private String procedure;

        public ViewHolder(View v) {
            super(v);
            recipeName = (TextView) v.findViewById(R.id.recipeName);
            duration = (TextView) v.findViewById(R.id.duration);
            ingredients = (TextView) v.findViewById(R.id.ingredients);
            rating = (RatingBar) v.findViewById(R.id.recipeRating);
            nutrients = (TextView) v.findViewById(R.id.nutrients);
            difficulty = (TextView) v.findViewById(R.id.difficulty);
        }

        public TextView getRecipeNameText() {
            return recipeName;
        }

        public TextView getDuration() {
            return duration;
        }

        public TextView getIngredients() {
            return ingredients;
        }

        public RatingBar getRating() {
            return rating;
        }

        public TextView getNutrients() {
            return nutrients;
        }

        public TextView getDifficulty() {
            return difficulty;
        }
    }

}
