package it.cocito.chef.fragments.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import it.cocito.chef.R;
import it.cocito.chef.dietElements.DailySchedule;
import it.cocito.chef.fragments.DietFragment;
import it.cocito.chef.fragments.dialogs.InfoDailyScheduleDialogFragment;

public class DailySchedulesAdapter extends RecyclerView.Adapter<DailySchedulesAdapter.ViewHolder> {

    private static final String TAG = "DailySchedulesAdapter";

    private String folder;
    private ArrayList<DailySchedule> dataSet = new ArrayList<>();
    private DietFragment fragment;

    public DailySchedulesAdapter(ArrayList<DailySchedule> dataSet, String folder, DietFragment fragment) {
        this.folder = folder;
        this.fragment = fragment;
        for (DailySchedule ds : dataSet)
            addItem(ds);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup vg, int type) {
        View v = LayoutInflater.from(vg.getContext()).inflate(R.layout.text_row_item, vg, false);
        return new ViewHolder(v, fragment, dataSet);
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, final int position) {
        vh.getTextView().setText(dataSet.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void addItem(DailySchedule schedule) {
        try {
            dataSet.add(getItemCount(), schedule);
            File file = new File(folder + schedule.getName() + ".json");
            Log.d("test", file.getPath().toString());
            file.getParentFile().mkdirs();
            file.createNewFile();
            schedule.WriteToJson(new FileOutputStream(file));
            notifyItemInserted(getItemCount() - 1);
            notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeItem(int i) {
        DailySchedule schedule = dataSet.get(i);
        File file = new File(folder + schedule.getName() + ".json");
        file.delete();
        dataSet.remove(i);
        notifyItemInserted(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void updateItem(int id, DailySchedule schedule) {
        try {
            File file = new File(folder + dataSet.get(id).getName() + ".json");
            file.delete();
            dataSet.set(id, schedule);
            File file2 = new File(folder + schedule.getName() + ".json");
            file2.getParentFile().mkdirs();
            file2.createNewFile();
            schedule.WriteToJson(new FileOutputStream(file2));
            notifyItemInserted(getItemCount() - 1);
            notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v, final DietFragment f, final ArrayList<DailySchedule> schedules) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InfoDailyScheduleDialogFragment frag = new InfoDailyScheduleDialogFragment();
                    frag.setFragment(f);
                    frag.setID(getAdapterPosition());
                    frag.setSchedule(schedules.get(getAdapterPosition()));
                    frag.show(f.getFragmentManager(), "info");
                }
            });
            textView = (TextView) v.findViewById(R.id.textView);
        }

        public TextView getTextView() {
            return textView;
        }
    }

}
