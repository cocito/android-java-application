package it.cocito.chef.fragments;

import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import it.cocito.chef.R;
import it.cocito.chef.dietElements.DailySchedule;
import it.cocito.chef.fragments.adapter.DailySchedulesAdapter;
import it.cocito.chef.fragments.dialogs.ModifyDailyScheduleDialogFragment;

public class DietFragment extends Fragment {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 2;

    protected LayoutManagerType type;
    private RecyclerView recycler;
    private DailySchedulesAdapter adapter;
    private RecyclerView.LayoutManager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup vg, Bundle savedState) {
        View v = inf.inflate(R.layout.diet_fragment, vg, false);

        recycler = (RecyclerView) v.findViewById(R.id.blocks_list);
        manager = new LinearLayoutManager(getActivity());
        type = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedState != null)
            type = (LayoutManagerType) savedState.getSerializable(KEY_LAYOUT_MANAGER);
        setRecyclerViewLayoutManager(type);

        adapter = new DailySchedulesAdapter(initDataSet(), getActivity().getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString() + File.separatorChar + "blocks" + File.separatorChar, this);
        recycler.setAdapter(adapter);

        DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(), DividerItemDecoration.VERTICAL);
        recycler.addItemDecoration(divider);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Dichiaro le costanti dello schermo sfruttando il WindowManager
        Point pt = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(pt);
        int width = pt.x;
        int height = pt.y;

        int size = (int) (width * 0.2); // 20% della grandezza
        //DEBUGGING INFO
        //System.out.println("[LOGGING SYSTEM] Width: " + width + " | Height: " + height + " | Size: " + size);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            size = (int) (size * (float) (height) / (float) (width)); // Se il telefono è in orizzontale, allora riscalo

        Button btnAdd = (Button) getView().findViewById(R.id.btnAdd);

        ViewGroup.LayoutParams params = btnAdd.getLayoutParams();
        // Imposto le dimensioni uguali entrambe al 20% della larghezza dello schermo
        params.width = size;
        params.height = size;
        //DEBUGGING INFO
        //System.out.println("[LOGGING SYSTEM] Width: " + width + " | Height: " + height + " | Size: " + size);
        btnAdd.setLayoutParams(params); // Reinserisco i parametri nel bottone

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifyDailyScheduleDialogFragment frag = new ModifyDailyScheduleDialogFragment();
                frag.setFragment(DietFragment.this);
                frag.show(getFragmentManager(), "info");
            }
        });
    }

    public void setRecyclerViewLayoutManager(LayoutManagerType lmt) {
        int scrollPosition = 0;

        if (recycler.getLayoutManager() != null)
            scrollPosition = ((LinearLayoutManager) recycler.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

        switch (lmt) {
            case GRID_LAYOUT_MANAGER:
                manager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                type = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                manager = new LinearLayoutManager(getActivity());
                type = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                manager = new LinearLayoutManager(getActivity());
                type = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        recycler.setLayoutManager(manager);
        recycler.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, type);
        super.onSaveInstanceState(savedInstanceState);
    }

    public ArrayList<DailySchedule> initDataSet() {
        ArrayList<DailySchedule> dataSet = null;
        try {
            dataSet = new ArrayList<>();
            File dir = new File(getActivity().getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString() + File.separatorChar + "blocks" + File.separatorChar);
            int i = 0;
            if (dir.listFiles() != null && dir.listFiles().length > 0)
                for (File f : dir.listFiles()) {
                    i++;
                    DailySchedule tmp = new DailySchedule();
                    Log.d("test", i + " | " + dir.listFiles().length);
                    tmp.ReadFromJson(new FileInputStream(f));
                    dataSet.add(tmp);
            }
        }catch (IOException e){
            Log.d("",e.getMessage());
        }
        return dataSet;
    }

    public void addItem(DailySchedule schedule) {
        adapter.addItem(schedule);
    }

    public void removeItem(int ID) {
        adapter.removeItem(ID);
    }

    public void updateItem(int ID, DailySchedule schedule) {
        adapter.updateItem(ID, schedule);
    }

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
}
