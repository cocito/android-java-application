package it.cocito.chef.fragments.dialogs;

import android.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import it.cocito.chef.MainActivity;
import it.cocito.chef.R;
import it.cocito.chef.dietElements.DailySchedule;
import it.cocito.chef.fragments.DietFragment;
import it.cocito.chef.fragments.adapter.MealBlocksAdapter;

public class MealBlocksFragment extends Fragment {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 2;

    protected LayoutManagerType type;
    private RecyclerView recycler;
    private MealBlocksAdapter adapter;
    private RecyclerView.LayoutManager manager;
    private DailySchedule schedule;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup vg, Bundle savedInstanceState) {
        View v = inf.inflate(R.layout.mealblock_fragment, vg, false);

        recycler = (RecyclerView) v.findViewById(R.id.meal_blocks_list);
        manager = new LinearLayoutManager(getActivity());
        type = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null)
            type = (LayoutManagerType) savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER);
        setRecyclerViewLayoutManager(type);

        adapter = new MealBlocksAdapter(schedule.getContent(), this);
        recycler.setAdapter(adapter);

        DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(), DividerItemDecoration.VERTICAL);
        recycler.addItemDecoration(divider);

        v.findViewById(R.id.backBtn_meal_block).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).pushFrag(new DietFragment());
            }
        });

        v.findViewById(R.id.saveBtn_meal_block).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String folder = getActivity().getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString() + File.separatorChar + "blocks" + File.separatorChar;
                    File file = new File(folder + schedule.getName() + ".json");
                    file.delete();
                    file.createNewFile();
                    schedule.WriteToJson(new FileOutputStream(file));
                    ((MainActivity) getActivity()).pushFrag(new DietFragment());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        return v;
    }

    public void setSchedule(DailySchedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Dichiaro le costanti dello schermo sfruttando il WindowManager
        Point pt = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(pt);
        int width = pt.x;
        int height = pt.y;

        int size = (int) (width * 0.2); // 20% della grandezza
        //DEBUGGING INFO
        //System.out.println("[LOGGING SYSTEM] Width: " + width + " | Height: " + height + " | Size: " + size);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            size = (int) (size * (float) (height) / (float) (width)); // Se il telefono è in orizzontale, allora riscalo

        Button btnAdd = (Button) getView().findViewById(R.id.btnAdd_meal_block);

        ViewGroup.LayoutParams params = btnAdd.getLayoutParams();
        // Imposto le dimensioni uguali entrambe al 20% della larghezza dello schermo
        params.width = size;
        params.height = size;
        //DEBUGGING INFO
        //System.out.println("[LOGGING SYSTEM] Width: " + width + " | Height: " + height + " | Size: " + size);
        btnAdd.setLayoutParams(params); // Reinserisco i parametri nel bottone
/*
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifyDailyScheduleDialogFragment frag = new ModifyDailyScheduleDialogFragment();
                frag.setFragment(MealBlocksFragment.this);
                frag.show(getFragmentManager(), "info");
            }
        });*/
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, type);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setRecyclerViewLayoutManager(MealBlocksFragment.LayoutManagerType lmt) {
        int scrollPosition = 0;

        if (recycler.getLayoutManager() != null)
            scrollPosition = ((LinearLayoutManager) recycler.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

        switch (lmt) {
            case GRID_LAYOUT_MANAGER:
                manager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                type = MealBlocksFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                manager = new LinearLayoutManager(getActivity());
                type = MealBlocksFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                manager = new LinearLayoutManager(getActivity());
                type = MealBlocksFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        recycler.setLayoutManager(manager);
        recycler.scrollToPosition(scrollPosition);
    }

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
}
