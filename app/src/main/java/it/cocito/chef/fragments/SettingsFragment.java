package it.cocito.chef.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.cocito.chef.R;

public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup vg, Bundle savedState) {

        return inf.inflate(R.layout.settings_fragment, vg, false);
    }
}
