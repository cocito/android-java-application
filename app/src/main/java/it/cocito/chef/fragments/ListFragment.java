package it.cocito.chef.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import it.cocito.chef.R;
import it.cocito.chef.Recipe;
import it.cocito.chef.Recognize;
import it.cocito.chef.fragments.adapter.RecipeAdapter;
import it.cocito.chef.recipeDatabase.RecipeDatabase;
import it.cocito.chef.recipeDatabase.RecipeParsingExceptionHandling;
import it.cocito.chef.serverConnection.InvalidQueryException;
import it.cocito.chef.serverConnection.ServerConnectionFailureException;

public class ListFragment extends Fragment {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 2;
    private static final int PERMISSION_RECORD_AUDIO = 1001;
    protected ListFragment.LayoutManagerType type;
    private RecyclerView recycler;
    private RecipeAdapter adapter;
    private RecyclerView.LayoutManager manager;
    private ArrayList<Recipe> dataSet = new ArrayList<Recipe>();
    private SearchView searchByNameView;
    private Recognize recognize;

    private boolean discreteMode;

    @Override
    public void setArguments(Bundle bundle) {
        discreteMode = (Boolean) bundle.get("DiscreteMode");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup vg, Bundle savedState) {
        View v = inf.inflate(R.layout.list_fragment, vg, false);

        recycler = (RecyclerView) v.findViewById(R.id.recipes_list);
        manager = new LinearLayoutManager(getActivity());
        type = ListFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        adapter = new RecipeAdapter(dataSet, discreteMode);
        recycler.setAdapter(adapter);

        //initDataSet();

        if (savedState != null)
            type = (ListFragment.LayoutManagerType) savedState.getSerializable(KEY_LAYOUT_MANAGER);
        setRecyclerViewLayoutManager(type);


        DividerItemDecoration divider = new DividerItemDecoration(recycler.getContext(), DividerItemDecoration.VERTICAL);
        recycler.addItemDecoration(divider);
        searchByNameView = (SearchView) v.findViewById(R.id.nameSearch);
        searchByNameView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                //setMenuVisibility(false);
                getSearchByNameView().setIconified(false);
                searchByName(query);
                return false;
            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recognize = new Recognize(getActivity(), "Non ho capito", searchByNameView);

        // Dichiaro le costanti dello schermo sfruttando il WindowManager
        Point pt = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(pt);
        int width = pt.x;
        int height = pt.y;

        int size = (int) (width * 0.2); // 20% della grandezza
        //DEBUGGING INFO
        //System.out.println("[LOGGING SYSTEM] Width: " + width + " | Height: " + height + " | Size: " + size);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            size = (int) (size * (float) (height) / (float) (width)); // Se il telefono è in orizzontale, allora riscalo


        Button micButton = (Button) getView().findViewById(R.id.microphone);
        micButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.RECORD_AUDIO)
                                != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_RECORD_AUDIO);
                        }
                        recognize.startListening();
                        break;
                    case MotionEvent.ACTION_UP:
                        recognize.stopListening();
                        break;
                }
                return false;
            }
        });
    }


    public void setRecyclerViewLayoutManager(ListFragment.LayoutManagerType lmt) {
        int scrollPosition = 0;

        if (recycler.getLayoutManager() != null)
            scrollPosition = ((LinearLayoutManager) recycler.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

        switch (lmt) {
            case GRID_LAYOUT_MANAGER:
                manager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                type = ListFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                manager = new LinearLayoutManager(getActivity());
                type = ListFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                manager = new LinearLayoutManager(getActivity());
                type = ListFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        recycler.setLayoutManager(manager);
        recycler.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, type);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void searchByName(String query) {
        new SearchRecipeTask().execute(query.split(";"));
    }

    private SearchView getSearchByNameView() {
        return searchByNameView;
    }

    public void setDiscreteMode(boolean discreteMode) {
        this.discreteMode = discreteMode;
    }

    /*public void initDataSet() {
        try {
            List<Recipe> recipes = RecipeDatabase.getRecipesFromName("Spaghetti", 10, RecipeParsingExceptionHandling.Discard_Details);
            dataSet.addAll(recipes);
        }
        catch (ServerConnectionFailureException | NetworkOnMainThreadException e){
            Toast.makeText(getActivity(), "No internet",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        catch (InvalidQueryException e){
            e.printStackTrace();
        }

    }*/

    /*public void addItem(String name, String days) {
        Log.d("ListFragmet", "Element " + adapter.getItemCount() + " set.");
        DailySchedule schedule = new DailySchedule(name, true, null, days);
        adapter.addItem(schedule);
    }*/

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    /*static void showDialog(FragmentManager fragmentManager) {

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = new DialogListFragment();
        newFragment.show(ft, "dialog");
    }*/

    private class SearchRecipeTask extends AsyncTask<String, Integer, List<Recipe>> {
        protected List<Recipe> doInBackground(String... query) {
            try {
                Log.d("ListFragment", "In arrivo " + query.length + " elementi");
                if (query.length == 0) {
                    return null;
                } else if (query.length == 1) {
                    return RecipeDatabase.getRecipesFromName(query[0], 10, RecipeParsingExceptionHandling.Discard_Details);
                } else {
                    return RecipeDatabase.getRecipesFromIngredients(query, 1, 10, RecipeParsingExceptionHandling.Discard_Details);
                }
            } catch (ServerConnectionFailureException | InvalidQueryException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            //setProgressPercent(progress[0]);
        }

        protected void onPostExecute(List<Recipe> result) {
            if (result == null) {
                Toast.makeText(getActivity(), "Nessuna ricetta trovata", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(getActivity(), "Finish", Toast.LENGTH_SHORT).show();
                dataSet.clear();
                dataSet.addAll(result);
                adapter.notifyDataSetChanged();
                //recycler.invalidate();
                //synchronized(adapter) {
                //    //adapter.notifyDataSetChanged();
                //}
            }
        }
    }
}


