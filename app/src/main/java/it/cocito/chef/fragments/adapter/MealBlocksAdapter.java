package it.cocito.chef.fragments.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.cocito.chef.R;
import it.cocito.chef.Recipe;
import it.cocito.chef.dietElements.MealBlock;
import it.cocito.chef.fragments.dialogs.MealBlocksFragment;

public class MealBlocksAdapter extends RecyclerView.Adapter<MealBlocksAdapter.ViewHolder> {

    private List<MealBlock> dataSet = new ArrayList<>();
    private MealBlocksFragment fragment;

    public MealBlocksAdapter(List<MealBlock> dataSet, MealBlocksFragment fragment) {
        this.fragment = fragment;
        if (dataSet != null && dataSet.size() > 0)
            for (MealBlock b : dataSet)
                addItem(b);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.text_row_block, parent, false);
        return new ViewHolder(v, fragment, dataSet);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MealBlock mb = dataSet.get(position);
        holder.getName().setText(mb.getName());
        String msg = String.format("%02d", mb.getTime().getHour()) + ":" + String.format("%02d", mb.getTime().getMinutes()) + ":" + String.format("%02d", mb.getTime().getSeconds());
        holder.getTiming().setText(msg);
        msg = "";
        for (Recipe r : mb.getRecipes())
            msg = msg + r.getName() + "\n";
        msg = msg.substring(0, msg.length() - 2);
        holder.getRecipes().setText(msg);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void addItem(MealBlock mb) {
        dataSet.add(mb);
        notifyItemInserted(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void removeItem(int i) {
        dataSet.remove(i);
        notifyItemInserted(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public void updateItem(int id, MealBlock mb) {
        dataSet.set(id, mb);
        notifyItemInserted(getItemCount() - 1);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name, timing, recipes;

        public ViewHolder(View itemView, MealBlocksFragment fragment, List<MealBlock> dataSet) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.textView_block_name);
            timing = (TextView) itemView.findViewById(R.id.textView_block_time);
            recipes = (TextView) itemView.findViewById(R.id.textView_block_recipes);
        }

        public TextView getName() {
            return name;
        }

        public TextView getTiming() {
            return timing;
        }

        public TextView getRecipes() {
            return recipes;
        }
    }

}
