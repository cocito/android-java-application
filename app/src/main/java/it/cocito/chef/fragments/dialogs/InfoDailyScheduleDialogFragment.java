package it.cocito.chef.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import it.cocito.chef.MainActivity;
import it.cocito.chef.R;
import it.cocito.chef.dietElements.DailySchedule;
import it.cocito.chef.fragments.DietFragment;

public class InfoDailyScheduleDialogFragment extends DialogFragment {

    private int ID;
    private DailySchedule schedule;
    private DietFragment fragment;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.diet_info_dialog, null);

        view.findViewById(R.id.button_modify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModifyDailyScheduleDialogFragment frag = new ModifyDailyScheduleDialogFragment();
                frag.setDays(schedule.getDay());
                frag.setScheduleID(ID);
                frag.setScheduleName(schedule.getName());
                frag.setFragment(fragment);
                frag.setState(ModifyDailyScheduleDialogFragment.MOD);
                frag.show(getFragmentManager(), "info");
                dismiss();
            }
        });

        view.findViewById(R.id.button_modify_recipes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MealBlocksFragment mbf = new MealBlocksFragment();
                mbf.setSchedule(schedule);
                ((MainActivity) getActivity()).pushFrag(mbf);
                dismiss();
            }
        });

        ((TextView) view.findViewById(R.id.text_name_info_schedule)).setText(schedule.getName());
        ((TextView) view.findViewById(R.id.text_days_info_schedule)).setText(getDays());

        builder.setTitle("Info of " + schedule.getName())
                .setView(view)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        InfoDailyScheduleDialogFragment.this.getDialog().cancel();
                    }
                })
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new AlertDialog.Builder(getActivity()).setTitle("Confirm")
                                .setMessage("Do you really want to delete this schedule?")
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        fragment.removeItem(ID);
                                    }
                                })
                                .setNegativeButton("No", null).show();
                    }
                });

        return builder.create();
    }

    public void setFragment(DietFragment fragment) {
        this.fragment = fragment;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public DailySchedule getSchedule() {
        return this.schedule;
    }

    public void setSchedule(DailySchedule schedule) {
        this.schedule = schedule;
    }

    public String getDays() {
        String msg = "";
        if (schedule.getDay().length() > 0) {
            if (schedule.getDay().contains("Mon")) msg = msg + "Mon, ";
            if (schedule.getDay().contains("Tue")) msg = msg + "Tue, ";
            if (schedule.getDay().contains("Wed")) msg = msg + "Wed, ";
            if (schedule.getDay().contains("Thu")) msg = msg + "Thu, ";
            if (schedule.getDay().contains("Fri")) msg = msg + "Fri, ";
            if (schedule.getDay().contains("Sat")) msg = msg + "Sat, ";
            if (schedule.getDay().contains("Sun")) msg = msg + "Sun, ";
            msg = msg.substring(0, msg.length() - 2);
            msg = msg + ".";
        } else msg = "none.";
        return msg;
    }
}
