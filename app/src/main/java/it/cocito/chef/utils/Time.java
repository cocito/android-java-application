package it.cocito.chef.utils;

public class Time {
    private int hour, minutes, seconds;

    /**
     * Atribuisce un valore alle variaabili hour, minutes, seconds
     *
     * @param hh ore
     * @param mm minuti
     * @param ss secondi
     */
    public Time(int hh, int mm, int ss) {
        hour = hh;
        minutes = mm;
        seconds = ss;
        try {
            normalize();
        } catch (TimeFormatException tfe) {
            //TODO
        }
    }

    /**
     * Gestisce l'aumento delle ore dei minuti e dei secondi
     *
     * @throws TimeFormatException errore nel formato di time
     */
    public void normalize()
            throws TimeFormatException {
        if (getHour() < 0) throw new TimeFormatException("hour", getHour());
        if (getMinutes() < 0) throw new TimeFormatException("minutes", getMinutes());
        if (getSeconds() < 0) throw new TimeFormatException("seconds", getSeconds());
        if (getSeconds() > 59) {
            setSeconds(0);
            increase(1);
        }
        if (getMinutes() > 59) {
            setMinutes(0);
            increase(0);
        }
        if (getHour() > 23) setHour(0);
    }

    /**
     * Controlla che i valori non siano cambiati
     *
     * @param t istanza Time
     * @return true se le condizioni sono tutte vere altrimenti false
     */
    public boolean equals(Time t) {
        return getHour() == t.getHour() && getMinutes() == t.getMinutes() && getSeconds() == t.getSeconds();
    }

    // GETTERS & SETTERS

    /**
     * Prende il valore della variabile hour
     *
     * @return restituisce il valore della variabile hour
     */
    public int getHour() {
        return hour;
    }

    /**
     * Attribuisce un valore a hour
     *
     * @param hh ore, variabile di Time
     */
    public void setHour(int hh) {
        hour = hh;
    }

    /**
     * Prende il valore della variabile minutes
     *
     * @return restituisce il valore della variabile minutes
     */
    public int getMinutes() {
        return minutes;
    }

    /**
     * Attribuisce un valore a minuter
     *
     * @param mm minuti, variabile di Time
     */
    public void setMinutes(int mm) {
        minutes = mm;
    }

    /**
     * Prende il valore della variabile seconds
     *
     * @return restituisce il valore della variabile seconds
     */
    public int getSeconds() {
        return seconds;
    }

    /**
     * Attribuisce un valore a seconds
     *
     * @param ss secondi, variabile di Time
     */
    public void setSeconds(int ss) {
        seconds = ss;
    }

    /**
     * Metodo di utilità per increamentare le variabili basandosi sul fattore par. Creato per evitare di fare un metodo increase per ogni attributo
     *
     * @param par -> 0 > Ore | 1 > Minuti | 2 -> Secondi
     */
    public void increase(int par) {
        switch (par) {
            case 0:
                setHour(getHour() + 1);
                break;
            case 1:
                setMinutes(getMinutes() + 1);
                break;
            case 2:
                setSeconds(getSeconds() + 1);
                break;
        }
    }
}
