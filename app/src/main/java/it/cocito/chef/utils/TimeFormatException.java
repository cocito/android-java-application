package it.cocito.chef.utils;

public class TimeFormatException extends Exception {

    /**
     * Indica se ci sono problemi con il formato di Time
     *
     * @param par parametro su cui vi è il problema (ore, minuti o secondi)
     * @param val valore attuae del parametro
     */
    public TimeFormatException(String par, int val) {
        super("The program has thrown and exception caused by an invalid value of the parameter " + par + "with OOB value of " + val + ".");
    }

}
