package it.cocito.chef.serverConnection;

public class InvalidParameterFormatException extends RuntimeException {
    /**
     * Indica quando un parametro non rispetta il proprio formato
     *
     * @param parameter parametro preso in considerazione
     */
    public InvalidParameterFormatException(String parameter) {
        super("The parameter \"" + parameter + "\" doesn't follow the valid parameter format. Please use the \"parameterName=parameterValue\" format.");
    }
}
