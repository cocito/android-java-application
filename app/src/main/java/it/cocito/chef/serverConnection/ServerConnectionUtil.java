package it.cocito.chef.serverConnection;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public final class ServerConnectionUtil {
    /**
     * Crea un'istanza di ServerConnectionUtil
     */
    private ServerConnectionUtil() {

    }

    /**
     * Invia una richiesta a un server e converte il risultato JSON in una Map<String,Object>
     *
     * @param url        L'URL del pagina, escludendo la parte dei parametri
     *                   (es. http://google.com va bene, http://google.com? e http://google.com?q=ciao non vanno bene)
     * @param parameters I parametri della query, nel formato "nomeParametro=valoreParametro"
     * @return
     * @throws ServerConnectionFailureException fallita connessione al server
     * @throws MalformedURLException errata composizione dell'URL
     */
    public static java.util.Map<String, Object> getMapResponse(String url, String... parameters)
            throws ServerConnectionFailureException, MalformedURLException, InvalidParameterFormatException {
        return (Map<String, Object>) getObjectResponse(url, new HashMap<String, Object>(), parameters);
    }

    /**
     * @param url        L'URL della pagina
     * @param baseObject
     * @param parameters
     * @return
     * @throws ServerConnectionFailureException fallita connessione al server
     * @throws MalformedURLException            errata composizione del'URL
     * @throws InvalidParameterFormatException  parametro che non rispetta il formato
     */
    public static Object getObjectResponse(String url, Object baseObject, String... parameters)
            throws ServerConnectionFailureException, MalformedURLException, InvalidParameterFormatException {
        Gson gson = new Gson();
        String json = sendGetRequest(url, parameters);

        return gson.fromJson(json, baseObject.getClass());
    }

    /**
     * Invia una rischiesta di tipo GET al server
     *
     * @param url L'URL del pagina, escludendo la parte dei parametri
     *            (es. http://google.com va bene, http://google.com? e http://google.com?q=ciao non vanno bene)
     * @return
     * @throws MalformedURLException            errata composizione dell'URL
     * @throws ServerConnectionFailureException fallita connessione al server
     */
    private static String sendGetRequest(String url)
            throws MalformedURLException, ServerConnectionFailureException {

        StringBuilder result = new StringBuilder();
        URL obj = new URL(url);
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(obj.openStream()));

            String strTemp;

            while ((strTemp = br.readLine()) != null) {
                result.append(strTemp + "\n");
            }
        } catch (IOException e) {
            throw new ServerConnectionFailureException(url, e);
        }

        return result.toString();
    }


    /**
     * Invia una richiesta GET sanitizzando i parametri
     *
     * @param url        L'URL del pagina, escludendo la parte dei parametri
     *                   (es. http://google.com va bene, http://google.com? e http://google.com?q=ciao non vanno bene)
     * @param parameters I parametri della query, nel formato "nomeParametro=valoreParametro"
     * @return
     * @throws MalformedURLException errata composizione dell'URL
     * @throws ServerConnectionFailureException fallita connessione al server
     */
    public static String sendGetRequest(String url, String... parameters)
            throws MalformedURLException, ServerConnectionFailureException {

        //Se non ci sono dei parametri fai la richiesta normalmente
        if (parameters.length == 0) {
            return sendGetRequest(url);
        }

        String sanitisedParameterString = "?";

        //Sanitizza i parametri
        for (int i = 0; i < parameters.length; i++) {
            try {
                String[] parameterParts = parameters[i].split("=");

                if (parameterParts.length != 2) {
                    throw new InvalidParameterFormatException(parameters[i]);
                }

                sanitisedParameterString += URLEncoder.encode(parameterParts[0], "UTF-8") + "=" + URLEncoder.encode(parameterParts[1], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError("UTF-8 is unknown.");
            }
            //Aggiungi un "&" tra i parametri
            if (i < parameters.length - 1) {
                sanitisedParameterString += "&";
            }
        }

        return sendGetRequest(url + sanitisedParameterString);
    }
}
