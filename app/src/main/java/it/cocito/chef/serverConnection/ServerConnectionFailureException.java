package it.cocito.chef.serverConnection;

public class ServerConnectionFailureException extends Exception {
    /**
     * Indica quando fallisce un tentativo di connessione al server
     *
     * @param url indirizzo della risora a cui si tentava di accedere
     */
    public ServerConnectionFailureException(String url) {
        super("Failed to connect to the server while visiting the URL \"" + url + "\"");
    }

    /**
     * Indica quando fallisce un tentativo di connessione al server
     * specificandone la causa
     *
     * @param url   indirizzo della risora a cui si cercava di accedere
     * @param cause causa del'errore durante il tentativo di connessione
     */
    public ServerConnectionFailureException(String url, Throwable cause) {
        super("Failed to connect to the server while visiting the URL \"" + url + "\": " + cause.getMessage(), cause);
    }
}
