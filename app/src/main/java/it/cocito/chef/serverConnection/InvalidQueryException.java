package it.cocito.chef.serverConnection;


public class InvalidQueryException extends Exception {
    /**
     * Indica quando una ricerca non è valida
     *
     * @param query ricerca effettuata che contine un problema
     */
    public InvalidQueryException(String query) {
        super("The query \"" + query + "\" is invalid.");
    }

    /**
     * Indica quando una ricerca non è valida specificandone
     * la motivazione
     *
     * @param query ricerca effettuata che contiene un problema
     * @param cause causa del problema riscontrato
     */
    public InvalidQueryException(String query, Throwable cause) {
        super("The query \"" + query + "\" is invalid.", cause);
    }
}
