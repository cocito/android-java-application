package it.cocito.chef;

import org.junit.Assert;
import org.junit.Test;

import it.cocito.chef.recipeDatabase.InvalidUnitException;
import it.cocito.chef.recipeDatabase.QueryParser;
import it.cocito.chef.serverConnection.ServerConnectionFailureException;

public class QueryParserTest {
    @Test
    public void QPParseUnitTest() throws InvalidUnitException, ServerConnectionFailureException {
        Object actual = QueryParser.parseUnit("fecola cucchiaio colmo");
        Assert.assertEquals(20f, actual);
    }

    @Test
    public void QPCleanQueryTest() {
        Object test1 = QueryParser.cleanQuery("Nel mezzo del cammin di nostra vita", 4);
        Assert.assertEquals("mezzo cammin nostra vita", test1);

        Object test2 = QueryParser.cleanQuery("Hey, I've just met you, and this is crazy", 4);
        Assert.assertEquals("just this crazy", test2);
    }
}
