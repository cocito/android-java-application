package it.cocito.chef;

import org.junit.Assert;
import org.junit.Test;

import it.cocito.chef.recipeDatabase.RecipeDatabase;
import it.cocito.chef.recipeDatabase.RecipeParsingExceptionHandling;
import it.cocito.chef.serverConnection.InvalidQueryException;
import it.cocito.chef.serverConnection.ServerConnectionFailureException;

public class RecipeDatabaseTest {
    @Test
    public void RDBgetRecipeFromNameTest()
            throws ServerConnectionFailureException, InvalidQueryException {
        RecipeParsingExceptionHandling recipeParsingExceptionHandling = RecipeParsingExceptionHandling.Discard_Details;
        Recipe spaghettiAlPomodoro = RecipeDatabase.getRecipesFromName("Spaghetti pomodoro", 5, recipeParsingExceptionHandling).get(0);
        Assert.assertEquals("Spaghetti al pomodoro", spaghettiAlPomodoro.getName());
        Assert.assertEquals(30f, (Object) spaghettiAlPomodoro.getTime());
    }

    @Test
    public void RDBgetRecipeFromIngredientsTest()
            throws ServerConnectionFailureException, InvalidQueryException {
        String[] ingredients = new String[]{
                "spaghetti", "pomodori", "cipolle", "aglio", "basilico", "olio extravergine di oliva", "parmigiano", "sale", "pepe"
        };

        RecipeParsingExceptionHandling recipeParsingExceptionHandling = RecipeParsingExceptionHandling.Discard_Details;
        Recipe spaghettiAlPomodoro = RecipeDatabase.getRecipesFromIngredients(ingredients, 0.2f, 5, recipeParsingExceptionHandling).get(0);
        Assert.assertEquals("Spaghetti al pomodoro", spaghettiAlPomodoro.getName());
        Assert.assertEquals(30f, (Object) spaghettiAlPomodoro.getTime());
    }
}
