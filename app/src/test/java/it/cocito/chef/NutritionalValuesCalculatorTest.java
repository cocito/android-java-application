package it.cocito.chef;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

import it.cocito.chef.nutritionalValues.NutrientRange;
import it.cocito.chef.nutritionalValues.NutritionalValuesCalculator;
import it.cocito.chef.nutritionalValues.UserPhysicalStats;

import static it.cocito.chef.nutritionalValues.NutritionalValuesCalculator.getDailyKcalRequirement;
import static it.cocito.chef.nutritionalValues.NutritionalValuesCalculator.getIngredient;
import static it.cocito.chef.nutritionalValues.NutritionalValuesCalculator.getNutritionalRequirements;
import static org.junit.Assert.fail;

public class NutritionalValuesCalculatorTest {
    @Test
    public void NVCgetIngredientTest() throws Exception {
        //Controlla le proteine e le calorie del pomodoro (test generico)
        Ingredient ingredient = getIngredient("pomodoro", 100);
        Assert.assertEquals(0.8f, (Object) ingredient.nutritionalValues.get("Proteine"));
        Assert.assertEquals(21f, (Object) ingredient.nutritionalValues.get("Valore_energetico"));

        //Controlla la vitamina D della salsiccia viennese (test parole multiple e approssimazione)
        Object vitaminD = getIngredient("salsiccia viennese", 100).nutritionalValues.get("Vitamina_D");
        Assert.assertEquals(0.000001f, vitaminD);
    }

    @Test
    public void NVCgetDailyKcalRequirementTest() throws Exception {

        //Donna giovane che fa jogging
        UserPhysicalStats stats1 =
                new UserPhysicalStats(UserPhysicalStats.Gender.Female, 25, 175, 56, UserPhysicalStats.ActivityLevel.Light);
        Assert.assertEquals((Object) 1881.0f, NutritionalValuesCalculator.getDailyKcalRequirement(stats1).getIdealValue());

        //Muratore di mezza età
        UserPhysicalStats stats2 =
                new UserPhysicalStats(UserPhysicalStats.Gender.Male, 35, 185, 68, UserPhysicalStats.ActivityLevel.Heavy);
        Assert.assertEquals((Object) 2874.0f, NutritionalValuesCalculator.getDailyKcalRequirement(stats2).getIdealValue());

        //Vecchietto che guarda Grande Fratello
        UserPhysicalStats stats3 =
                new UserPhysicalStats(UserPhysicalStats.Gender.Male, 85, 180, 70, UserPhysicalStats.ActivityLevel.Sedentary);
        Assert.assertEquals((Object) 1686.0f, NutritionalValuesCalculator.getDailyKcalRequirement(stats3).getIdealValue());
    }

    @Test
    public void NVCgetNutritionalRequirementsTest() throws Exception {
        Map<String, Float> nutritionalValues = NutritionalValuesCalculator.getIngredient("pomodoro", 100).nutritionalValues;

        UserPhysicalStats stats =
                new UserPhysicalStats(UserPhysicalStats.Gender.Female, 25, 175, 56, UserPhysicalStats.ActivityLevel.Light);

        NutrientRange kcalRequirement = getDailyKcalRequirement(stats);

        Map<String, NutrientRange> nutritionalRequirements = getNutritionalRequirements(stats, kcalRequirement);

        for (Map.Entry<String, Float> entry : nutritionalValues.entrySet()) {
            if (!nutritionalRequirements.keySet().contains(entry.getKey())) {
                fail("There is a nutritionalValue that doesn't appear in nutritionalRequirements: " + entry.getKey());
            }
        }
        for (Map.Entry<String, NutrientRange> entry : nutritionalRequirements.entrySet()) {
            if (!nutritionalValues.keySet().contains(entry.getKey())) {
                fail("There is a nutritionalRequirement that doesn't appear in nutritionalValues: " + entry.getKey());
            }
        }
    }

}
